import {UrlValidation} from "../helpers/validations/UrlValidation";
import {UrlFacebookValidation} from "../helpers/validations/UrlFacebookValidation";
import {UrlLinkedInValidation} from "../helpers/validations/UrlLinkedInValidation";
import {UrlTwitterValidation} from "../helpers/validations/UrlTwitterValidation";

export class SocialMedia {
    static check(value) {
        if (SocialMedia.isFacebook(value)) {
            return 'facebook';
        }

        if (SocialMedia.isTwitter(value)) {
            return 'twitter';
        }

        if (SocialMedia.isLinkedIn(value)) {
            return 'linkedIn';
        }

        if (SocialMedia.isUrl(value)) {
            return 'url';
        }

        return '';
    }

    static isFacebook(value) {
        return UrlFacebookValidation.validate(value);
    }

    static isTwitter(value) {
        return UrlTwitterValidation.validate(value);
    }

    static isLinkedIn(value) {
        return UrlLinkedInValidation.validate(value);
    }

    static isUrl(value) {
        return UrlValidation.validate(value);
    }
}
