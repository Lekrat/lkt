import {EmptyValidation} from "../../helpers/validations/EmptyValidation";
import {NotEmptyValidation} from "../../helpers/validations/NotEmptyValidation";
import {EmailValidation} from "../../helpers/validations/EmailValidation";
import {NumberValidation} from "../../helpers/validations/NumberValidation";
import {SpanishNifValidation} from "../../helpers/validations/SpanishNifValidation";
import {UrlValidation} from "../../helpers/validations/UrlValidation";
import {UrlLinkedInValidation} from "../../helpers/validations/UrlLinkedInValidation";
import {UrlTwitterValidation} from "../../helpers/validations/UrlTwitterValidation";
import {UrlFacebookValidation} from "../../helpers/validations/UrlFacebookValidation";

export class Validator {

    /**
     * @param validation
     */
    static addValidation (validation) {
        Validator.validations[validation.name] = validation;
    }

    /**
     * @returns {string[]}
     */
    static listAll () {
        return Object.keys(Validator.validations);
    }

    static hasValidation(validationName) {
        return Validator.listAll().includes(validationName) !== -1
    }

    /**
     * @param validationName
     * @param value
     * @returns {boolean}
     */
    static validate (validationName, value) {
        return Validator.validations[validationName] && Validator.validations[validationName].validate(value);
    }
}

Validator.validations = {};
Validator.addValidation(EmailValidation);
Validator.addValidation(EmptyValidation);
Validator.addValidation(NotEmptyValidation);
Validator.addValidation(NumberValidation);
Validator.addValidation(SpanishNifValidation);
Validator.addValidation(UrlValidation);
Validator.addValidation(UrlLinkedInValidation);
Validator.addValidation(UrlTwitterValidation);
Validator.addValidation(UrlFacebookValidation);