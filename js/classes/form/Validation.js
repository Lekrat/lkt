import {TypeHelper} from "../../helpers/TypeHelper";

export class Validation {
    constructor(name, fn){
        this.name = name;
        this.evaluator = TypeHelper.isFunction(fn) ? fn : () => { return true; };
    }

    validate(value){
        return this.evaluator(value);
    }
}