import {NavigatorHelper} from "../../helpers/NavigatorHelper";
import {Cookie} from "./engines/Cookie";
import {Storage} from "./engines/Storage";

export class Session {
    static set(name, value, expires) {
        if (NavigatorHelper.supportsLocalStorage() === true) {
            return Storage.set(name, value, expires);
        }
        return Cookie.set(name, value, expires);
    }

    static get(cname) {
        if (NavigatorHelper.supportsLocalStorage() === true) {
            return Storage.get(cname);
        }
        return Cookie.get(cname);
    }

    static del(name) {
        if (NavigatorHelper.supportsLocalStorage() === true) {
            return Storage.del(name);
        }
        return Cookie.del(name);
    }
}
