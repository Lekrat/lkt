export class Cookie {
    static set(name, value, expires) {
        let d = new Date();
        if (!expires) {
            expires = d.getTime() + (365 * 24 * 60 * 60 * 1000);
        }
        d.setTime(expires);
        expires = "expires=" + d.toUTCString();

        document.cookie = name + '=' + value + ', ' + expires;

        return true;
    }

    static get(cname) {
        let name = cname + "=";
        let ca = document.cookie.split(';');
        for (let i = 0; i < ca.length; i++) {
            let c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return '';
    }

    static del(name) {
        return this.set(name, '', -1);
    }
}
