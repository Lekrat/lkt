import {TypeHelper} from "../../../helpers/TypeHelper";
import {TimeHelper} from "../../../helpers/TimeHelper";

export class Storage {
    static set(name, value, expires) {
        let data = { value: value, expires: null };

        if (TypeHelper.isNumeric(expires)){
            data.expires = new Date(TimeHelper.time() + expires * 10000);
        }
        localStorage.setItem(name, JSON.stringify(data));
        return true;
    }

    static get(name) {

        let cached = JSON.parse(
            localStorage.getItem(name)
        );

        if (! cached) {
            return undefined;
        }

        if (TypeHelper.isNumeric(cached.expires) && new Date(cached.expires) < new Date()) {
            Storage.del(name);
            return undefined;
        }

        return cached.value;
    }

    static del(name) {
        return localStorage.removeItem(name);
    }
}
