export class StickyElement {
    constructor($el, stickySelector) {
        this.$el = $el;
        this.stickySelector = stickySelector;
        this.$body = document.getElementsByTagName('body')[0];
        this.position = StickyElement.getOffsetRect(this.$el);
    }

    refresh() {
        if (window.pageYOffset > this.position.top) {
            this.$body.classList.add(this.stickySelector);
        } else {
            this.$body.classList.remove(this.stickySelector);
        }
    }

    static getOffsetRect(el) {
        let rect = el.getBoundingClientRect();

        // add window scroll position to get the offset position
        let left = rect.left + window.scrollX,
            top = rect.top + window.scrollY,
            right = rect.right + window.scrollX,
            bottom = rect.bottom + window.scrollY,
            x = undefined,
            y = undefined;

        // polyfill missing 'x' and 'y' rect properties not returned
        // from getBoundingClientRect() by older browsers
        if (rect.x === undefined) {
            x = left;
        } else {
            x = rect.x + window.scrollX;
        }

        if (rect.y === undefined) {
            y = top;
        } else {
            y = rect.y + window.scrollY;
        }

        // width and height are the same
        let width = rect.width,
            height = rect.height;

        return {left, top, right, bottom, x, y, width, height};
    };
}