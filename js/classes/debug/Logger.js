import {FunctionHelper} from "../../helpers/FunctionHelper";

export const Logger = {

    data: {
        error: [],
        log: [],
        warn: [],
        catchError: [],
    },

    track(action = 'log', data = {}){
        Logger.data[action].push(data);
    },

    methods: {
        error: undefined,
        warn: undefined,
        log: undefined,
    },

    init(){
        // console.log
        Logger.methods.log = FunctionHelper.clone(console.log);
        console.log = (...args) => {
            Logger.track('log', args);
            return Logger.methods.log(args);
        };

        // console.error
        Logger.methods.error = FunctionHelper.clone(console.error);
        console.error = (...args) => {
            Logger.track('error', args);
            return Logger.methods.error(args);
        };

        // console.warn
        Logger.methods.warn = FunctionHelper.clone(console.warn);
        console.warn = (...args) => {
            Logger.track('warn', args);
            return Logger.methods.warn(args);
        };


        // window.onerror
        window.onerror = (error, url, line) => {
            Logger.track('catchError', {error, url, line});
        };
    }
};