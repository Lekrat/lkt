import {Router} from "./Router";
import {TypeHelper} from "../../helpers/TypeHelper";

export class Api {

    constructor(engine = 'axios') {
        this._engine = engine;
    }

    static engine(name = 'axios') {
        Api.instance._engine = name;
    }

    static call(routeName = '', args = {}, onSuccess = undefined, onError = undefined) {
        let data = Router.get(routeName).build(args);

        if (!TypeHelper.isFunction(onSuccess)) {
            onSuccess = (r) => {
                console.log('Route ends successfully', r);
            }
        }

        if (!TypeHelper.isFunction(onError)) {
            onError = (r) => {
                console.log('Route ends with errors', r);
            }
        }

        let validateStatus = (status) => {
            return true;
        };

        if (Api.instance._engine === 'axios') {
            // Execute call
            switch (data.method) {
                case 'get':
                case 'post':
                case 'put':
                case 'delete':
                    return axios(
                        {
                            method: data.method,
                            url: data.link,
                            validateStatus: validateStatus,
                            data: data.params
                        }).then(r => {
                        return Api.manageApiResponse(r, onSuccess, onError)
                    }).catch(error => {
                        return onError(error);
                    });
                case 'open':
                    return axios.get(link, {'responseType': 'blob'}).then(r => {
                        window.download(r.data, data.name);
                        return onSuccess(r)
                    }).catch(error => {
                        return onError(error);
                    });

                default:
                    console.warn('Error: Invalid method');
            }
        }
    }

    static manageAxiosResponse(r, onSuccess, onError) {
        if (r.status >= 200 && r.status < 300) {
            return onSuccess(r);
        } else {
            return onError(r);
        }
    }
}

Api.instance = new Api('axios');