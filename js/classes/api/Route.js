import {Router} from "./Router";
import {RouteHelper} from "./RouteHelper";
import {ObjectHelper} from "../../helpers/ObjectHelper";
import {TypeHelper} from "../../helpers/TypeHelper";
import {StringHelper} from "../../helpers/StringHelper";

export class Route {

    constructor(name = '', path = '', method = 'get') {
        this.name = name;
        this.path = path;
        this.method = method.toLowerCase();
        this.params = {};
        this.unsafeParams = false;
        this.renameParams = {};
        this.fillLeftSeparator = '{';
        this.fillRightSeparator = '}';
    }

    setFillUriPatter(leftSeparator = '{', rightReparator = '}') {
        this.fillLeftSeparator = leftSeparator;
        this.fillRightSeparator = rightReparator;
        return this;
    }

    enableUnsafeParams(enabled = true) {
        this.unsafeParams = enabled;
        return this;
    }

    param(name = "", defaultValue = undefined, rename = undefined) {
        this.params[name] = defaultValue;
        if (!TypeHelper.isUndefined(rename)) {
            this.renameParams[name] = rename;
        }
        return this;
    }

    build(args = {}) {
        let r = ObjectHelper.clone(this.params);

        for (let key in args) {
            if (this.unsafeParams || (args.hasOwnProperty(key) && this.params.hasOwnProperty(key))) {
                if (this.renameParams.hasOwnProperty(key)) {
                    delete r[key];
                    r[this.renameParams[key]] = args[key];
                } else {
                    r[key] = args[key];
                }

                if (TypeHelper.isUndefined(r[key])) {
                    delete r[key];
                }
            }
        }

        let toDelete = StringHelper.extractFillData(this.path, r, this.fillLeftSeparator, this.fillRightSeparator);
        let link = StringHelper.fill(this.path, r, this.fillLeftSeparator, this.fillRightSeparator);
        r = ObjectHelper.deleteKeys(r, toDelete);

        if (RouteHelper.hasToBuildQueryString(this.method)) {
            let stringParams = RouteHelper.paramsToString(r);
            link = [link, stringParams].join('?');
            r = {};
        }

        return {
            link,
            method: this.method,
            params: r,
        }
    }

    save() {
        Router.add(this);
    }

    static create(name = '', path = '', method = '') {
        return new Route(name, path, method);
    }
}