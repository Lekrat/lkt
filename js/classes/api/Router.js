export class Router {

    static add(route = undefined) {
        Router.routes[route.name] = route;
    }

    static get(routeName) {
        return Router.routes[routeName];
    }
}

Router.routes = {};