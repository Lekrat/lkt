export class RouteHelper {
    static paramsToString(params) {
        let r = [];
        for (let key in params) {
            if (params.hasOwnProperty(key)) {
                if (Array.isArray(params[key])) {
                    if (params[key].length > 0) {
                        r.push(key + '=' + JSON.stringify(params[key]) + '');
                    }
                } else {
                    r.push(key + '=' + params[key]);
                }
            }
        }

        return r.join('&');
    }

    static hasToBuildQueryString(method = '') {
        return method === 'get' || method === 'open';
    }
}