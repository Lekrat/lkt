import {DaysOfWeek} from "../translations/en/DaysOfWeek";
import {Months} from "../translations/en/Months";

export class Calendar {
    constructor() {
        let moment = new Date();
        this.moment = moment;
        this.currentMonth = moment.getMonth();
        this.currentYear = moment.getFullYear();
    }

    static setDaysOfWeek(daysOfWeek){
        Calendar.daysOfWeek = daysOfWeek;
    }

    static setMonths(months){
        Calendar.months = months;
    }

    getMonthName() {
        return Calendar.months[this.currentMonth];
    }

    static getDayOfWeek(date) {
        return Calendar.daysOfWeek[date.getDay()];
    }

    getMonthNumber(monthName) {
        switch (monthName) {
            case 'prev':
                return this.getPreviousCalendar().currentMonth;

            case 'next':
                return this.getNextCalendar().currentMonth;

            default:
                return this.currentMonth;
        }
    }

    daysInMonth() {
        return 32 - new Date(this.currentYear, this.currentMonth, 32).getDate();
    }

    next() {
        this.currentYear = (this.currentMonth === 11) ? this.currentYear + 1 : this.currentYear;
        this.currentMonth = (this.currentMonth + 1) % 12;
        return this;
    }

    previous() {
        this.currentYear = (this.currentMonth === 0) ? this.currentYear - 1 : this.currentYear;
        this.currentMonth = (this.currentMonth === 0) ? 11 : this.currentMonth - 1;
        return this;
    }

    jump(year, month) {
        this.currentYear = parseInt(year);
        this.currentMonth = parseInt(month);
        return this;
    }

    generate() {
        let firstDay = (new Date(this.currentYear, this.currentMonth)).getDay(),
            daysInMonth = this.daysInMonth(),
            r = [];

        // creating all cells
        let date = 1;
        for (let i = 0; i < 6; i++) {
            let dates = [];

            //creating individual cells, filing them up with data.
            for (let j = 0; j < 7; j++) {
                if (i === 0 && j < firstDay) {
                    continue;
                } else if (date > daysInMonth) {
                    break;
                } else {
                    dates.push(date);
                    ++date;
                }
            }

            if (dates.length > 0) {
                r[i] = dates;
            }
        }

        return r;
    }

    getPreviousCalendar() {
        return new Calendar().jump(this.currentYear, this.currentMonth).previous();
    }

    getNextCalendar() {
        return new Calendar().jump(this.currentYear, this.currentMonth).next();
    }

    generatePreviousMonth() {
        return this.getPreviousCalendar().generate();
    }

    generateNextMonth() {
        return this.getNextCalendar().generate();
    }

    toDate(day) {
        return new Date(this.currentYear, this.currentMonth, day);
    }

    formatSelectedValue(day){
        return new Date(this.currentYear, this.currentMonth, day);
    }

    show() {
        let r = this.generate().map((v) => {
                return v.map(v1 => {
                    let d = this.toDate(v1);
                    return {value: v1, label: v1, month: 'current', year: this.currentYear, selectedValue: d, date: d, dayOfWeek: Calendar.getDayOfWeek(d)}
                });
            }),
            l = r[0].length,
            limit = 0,
            temp = [],
            i = 0;

        // Fix previous month
        if (l < 7) {
            let prev = this.getPreviousCalendar();
            limit = prev.daysInMonth();
            i = (7 - l);
            temp = [];

            while (i > 0) {
                --i;
                let d = prev.toDate(limit - i);
                temp.push({value: (limit - i), label: (limit - i), month: 'previous', year: prev.currentYear, selectedValue: d, date: d, dayOfWeek: Calendar.getDayOfWeek(d)});
            }

            i = 0;
            while (i < l) {
                temp.push(r[0][i]);
                ++i;
            }

            r[0] = temp;
        }

        // Fix next month
        l = r[r.length - 1].length;
        if (l < 7) {
            i = l;
            --i;
            temp = r[r.length - 1];

            let next = this.getNextCalendar();
            let l1 = (7 - l);
            while (i < l1) {
                let d = next.toDate(i + 1);
                temp.push({value: (i + 1), label: (i + 1), month: 'next', year: next.currentYear, selectedValue: d, date: d, dayOfWeek: Calendar.getDayOfWeek(d)});
                ++i;
            }

            r[r.length - 1] = temp;
        }

        return r;
    }
}

Calendar.daysOfWeek = [];
Calendar.months = [];
Calendar.setDaysOfWeek(DaysOfWeek);
Calendar.setMonths(Months);