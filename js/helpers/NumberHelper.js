export const NumberHelper = {
    format(value, decimals, decimalPoint, thousandsSeparator) {
        let v = String(parseFloat(value).toFixed(decimals).replace(/\d(?=(\d{3})+\.)/g, '$&D'));
        v = String(v.replace('.', decimalPoint));
        v = v.replace(/D/g, thousandsSeparator);
        return v;
    },
    clean(value) {
        return value.replace(/\,/g, '').replace(/\./g, '').replace(/\D/g,'');
    }
};