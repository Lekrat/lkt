import {TypeHelper} from "./TypeHelper";
import {StringHelper} from "./StringHelper";

export const SocialShareHelper = {

    getMailUrl(email = undefined, subject = undefined, body = undefined) {
        let r = new URL('mailto:' + email);

        if (!TypeHelper.isUndefined(subject)) {
            r.searchParams.append('subject', subject);
        }

        if (!TypeHelper.isUndefined(body)) {
            r.searchParams.append('body', body);
        }

        return r.href;
    },

    getFacebookUrl(url = undefined) {
        if (TypeHelper.isUndefined(url)) {
            url = window.location.href;
        }
        let r = new URL('https://www.facebook.com/sharer/sharer.php');
        r.searchParams.append('u', url);
        return r.href;
    },

    getTwitterUrl(url = undefined, via = undefined, hashtags = []) {
        if (TypeHelper.isUndefined(url)) {
            url = window.location.href;
        }
        let r = new URL('https://twitter.com/intent/tweet');
        r.searchParams.append('url', url);

        if (!TypeHelper.isUndefined(via)){
            r.searchParams.append('via', via);
        }

        if (hashtags.length > 0){
            r.searchParams.append('hashtags', hashtags.join(' '));
        }
        return r.href;
    },

    getGooglePlusUrl(url = undefined) {
        if (TypeHelper.isUndefined(url)) {
            url = window.location.href;
        }
        let r = new URL('https://plus.google.com/share');
        r.searchParams.append('url', url);
        return r.href;
    },

    getLinkedInUrl(url = undefined, title = undefined, summary = undefined, source = undefined) {

        if (TypeHelper.isUndefined(url)) {
            url = window.location.href;
        }
        let r = new URL('https://www.linkedin.com/shareArticle');
        r.searchParams.append('mini', 'true');
        r.searchParams.append('url', StringHelper.htmlEntities(url));

        if (!TypeHelper.isUndefined(title)){
            r.searchParams.append('title', StringHelper.htmlEntities(title));
        }

        if (!TypeHelper.isUndefined(summary)){
            r.searchParams.append('summary', StringHelper.htmlEntities(summary));
        }

        if (!TypeHelper.isUndefined(source)){
            r.searchParams.append('source', StringHelper.htmlEntities(source));
        }
        return r.href;
    },

    getPinterestUrl(url = undefined, media = undefined, description = undefined) {

        if (TypeHelper.isUndefined(url)) {
            url = window.location.href;
        }
        let r = new URL('//es.pinterest.com/pin/create/button/');
        r.searchParams.append('url', url);

        if (!TypeHelper.isUndefined(media)){
            r.searchParams.append('media', media);
        }

        if (!TypeHelper.isUndefined(description)){
            r.searchParams.append('description', description);
        }

        return r.href;
    },

    getWhatsAppUrl(url = undefined, text = '') {

        if (TypeHelper.isUndefined(url)) {
            url = window.location.href;
        }

        text = StringHelper.htmlEntities(text) + ' ' + url;

        let r = new URL('whatsapp://send');
        r.searchParams.append('text', text);
        return r.href;
    }
};