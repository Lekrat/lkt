export class TypeHelper {

    static isArray(mixedVar) {
        if (typeof Array.isArray === 'undefined') {
            Array.isArray = function(obj) {
                return Object.prototype.toString.call(obj) === '[object Array]';
            }
        }

        return Array.isArray(mixedVar);
    }

    static isFunction(fn) {
        return typeof (fn) === 'function';
    }

    static isNumeric(value) {
        let v = String(value).replace(',', '.'),
            v1 = parseFloat(v);
        return !isNaN(v1)
            && isFinite(v1);
    }

    static isString(value) {
        return typeof value === 'string';
    }

    static isObject(mixedVar) {
        return typeof (mixedVar) === 'object';
    }

    static isUndefined(mixedVar) {
        return typeof (mixedVar) === 'undefined';
    }

    static is(v) {
        return typeof v !== "undefined" || v !== null;
    }

    static isset() {
        let a = arguments,
            l = a.length,
            i = 0,
            undef;

        if (l === 0) {
            throw new Error('Empty isset');
        }

        while (i !== l) {
            if (a[i] === undef || a[i] === null) {
                return false;
            }
            i++;
        }
        return true;
    }
}