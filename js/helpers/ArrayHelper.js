export const ArrayHelper = {
    count(t, e) {
        let r, n = 0;
        if (null === t || "undefined" == typeof t) {
            return 0;
        }
        if (t.constructor !== Array && t.constructor !== Object) {
            return 1;
        }
        "COUNT_RECURSIVE" === e && (e = 1), 1 != e && (e = 0);
        for (r in t) t.hasOwnProperty(r) && (n++, 1 != e || !t[r] || t[r].constructor !== Array && t[r].constructor !== Object || (n += this.count(t[r], 1)));
        return n
    },

    inArray(needle, haystack, argStrict) {
        let key = '';
        let strict = !!argStrict;
        if (strict) {
            for (key in haystack) {
                if (haystack[key] === needle) {
                    return key;
                }
            }
        } else {
            for (key in haystack) {
                if (haystack[key] == needle) {
                    return true;
                }
            }
        }
        return false
    },

    range(start, end) {
        let array = [];

        for (let i = start; i < end; i++) {
            array.push(i);
        }
        return array;
    },

    removeDuplicates(myArr, prop) {
        return myArr.filter((obj, pos, arr) => {
            return arr.map(mapObj => mapObj[prop]).indexOf(obj[prop]) === pos;
        });
    },

    getUniques(arr, comp) {
        return arr
            .map(e => e[comp])

            // store the keys of the unique objects
            .map((e, i, final) => final.indexOf(e) === i && i)

            // eliminate the dead keys & store unique objects
            .filter(e => arr[e]).map(e => arr[e]);
    },

    clone(arr) {
        return JSON.parse(JSON.stringify(arr));
    }
};