export class NavigatorHelper {

    static supportsPromises () {
        return typeof Promise !== "undefined" && Promise.toString().indexOf("[native code]") !== -1;
    }

    static supportsLocalStorage (){
        let supportsLocalStorage = 1;
        if (typeof localStorage === 'object') {
            try {
                localStorage.setItem('localStorage', 1);
                localStorage.removeItem('localStorage');
            } catch (e) {
                supportsLocalStorage = -1;
            }
        } else {
            supportsLocalStorage = -1;
        }

        return supportsLocalStorage === 1;
    }

    static supportsRgba () {
        let scriptElement = document.getElementsByTagName('script')[0],
            prevColor = scriptElement.style.color,
            r = true;
        try {
            scriptElement.style.color = 'rgba(1,5,13,0.44)';
            scriptElement.style.color = prevColor;
        } catch (e) {
            r = false;
        }
        return r;
    }

    static isAndroid () {
        return /(android)/i.test(navigator.userAgent);
    }

    static isAndroid5 () {
        return /(android 5)/i.test(navigator.userAgent);
    }

    static isSafari () {
        return /(safari)/i.test(navigator.userAgent) && !NavigatorHelper.isChrome();
    }

    static isChrome () {
        return /(chrome)/i.test(navigator.userAgent);
    }

    static isIPhone () {
        return /(iPhone|iPod)/i.test(navigator.userAgent);
    }

    static isIPad () {
        return /(iPad)/i.test(navigator.userAgent);
    }

    static isMobile () {
        return /(mobile|iPhone|iPod)/i.test(navigator.userAgent);
    }
}
