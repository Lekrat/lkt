import {Validation} from "../../classes/form/Validation";

export const UrlLinkedInValidation = new Validation('urlLinkedIn', function(value) {
    return new RegExp('(http(s?)://)*(?:www\\.)?.*?linkedin\\.com').test(value);
});