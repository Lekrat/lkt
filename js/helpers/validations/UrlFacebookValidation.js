import {Validation} from "../../classes/form/Validation";

export const UrlFacebookValidation = new Validation('urlFacebook', function(value) {
    return new RegExp('(http(s?)://)*(?:www\\.)?.*?facebook\\.com').test(value);
});