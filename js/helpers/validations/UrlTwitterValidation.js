import {Validation} from "../../classes/form/Validation";

export const UrlTwitterValidation = new Validation('urlTwitter', function(value) {
    return new RegExp('(http(s?)://)*(?:www\\.)?.*?twitter\\.com').test(value);
});