import {Validation} from "../../classes/form/Validation";

export const DateValidation = new Validation('date', function(value) {
    return value !== null
        && value !== undefined
        && (Object.prototype.toString.call(value) === "[object Date]"
            && !isNaN(value.getTime()));
});