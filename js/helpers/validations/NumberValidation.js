import {Validation} from "../../classes/form/Validation";
import {TypeHelper} from "../TypeHelper";

export const NumberValidation = new Validation('number', function(value) {
    return TypeHelper.isNumeric(value);
});