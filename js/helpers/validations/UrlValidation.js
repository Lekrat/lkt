import {Validation} from "../../classes/form/Validation";

export const UrlValidation = new Validation('url', function(value) {
    new RegExp('^(https?|chrome):\\/\\/[^\\s$.?#].[^\\s]*$').test(value)
});