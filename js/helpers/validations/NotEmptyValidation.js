import {Validation} from "../../classes/form/Validation";

export const NotEmptyValidation = new Validation('notEmpty', function (value) {
    if (typeof value === 'undefined') {
        return false;
    }
    return String(value) !== '';
});