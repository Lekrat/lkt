import {TypeHelper} from "./TypeHelper";

export class ObjectHelper {

    static clone(obj) {
        if (typeof obj === 'object') {
            return Object.assign( Object.create( Object.getPrototypeOf(obj)), obj);
        }
        return {};
    }

    static sort(obj) {
        // All browsers solution
        let r = [];

        for (let prop in obj) {

            let data = obj[prop];

            if (TypeHelper.isObject(data)) {
                data = ObjectHelper.sort(data);
            }

            r.push([prop, data]);
        }

        r.sort(function(a, b) {
            return a[0] - b[0];
        });

        let r1 = {};
        r.forEach(z => {
            r1[z[0]] = z[1];
        });

        return r1;


        // ES8 Solution
        // let r = Object.entries(obj)
        //     .sort(([,a],[,b]) => a-b)
        //     .reduce((r, [k, v]) => ({ ...r, [k]: v }), {});


        // ES10 Solution
        // let r = Object.fromEntries(
        //     Object.entries(data).sort(([,a],[,b]) => a-b)
        // );
    }

    static fetch(obj, key) {
        let args = key.split('.');
        let argsLength = args.length;
        let c = 0;
        let t = obj;

        // Parse config data and fetch attribute
        while (typeof (t[args[c]]) !== 'undefined') {
            t = t[args[c]];
            ++c;
        }

        // If not found...
        if (c < argsLength) {
            //t = '';
            t = null;
        }

        return t;
    }

    static merge (obj1, obj2) {
        for (let key in obj2) {
            if (obj2.hasOwnProperty(key)) {
                obj1[key] = obj2[key];
            }
        }
        return obj1;
    }

    static safeMerge (obj1, obj2) {
        for (let key in obj2) {
            if (obj1.hasOwnProperty(key) && obj2.hasOwnProperty(key)) {
                obj1[key] = obj2[key];
            }
        }
        return obj1;
    }

    static deleteKeys(obj, keys){
        keys.forEach(key => {
            if (obj.hasOwnProperty(key)){
                delete obj[key];
            }
        });

        return obj;
    }

    static toArray(t) {
        return Object.keys(t).map(function (e) {
            return t[e]
        })
    }

    static jsonDecode(str_json) {
        let json = window.JSON;

        if ("object" == typeof json && "function" == typeof json.parse) {
            try {
                return json.parse(str_json)
            } catch (err) {
                if (!(err instanceof SyntaxError)) {
                    throw new Error("Unexpected error type in ObjectHelper.jsonDecode()");
                }

                return null;
            }
        }

        let cx = /[\u0000\u00ad\u0600-\u0604\u070f\u17b4\u17b5\u200c-\u200f\u2028-\u202f\u2060-\u206f\ufeff\ufff0-\uffff]/g,
            j, text = str_json;

        return cx.lastIndex = 0, cx.test(text) && (text = text.replace(cx, function (t) {
            return "\\u" + ("0000" + t.charCodeAt(0).toString(16)).slice(-4)
        })), /^[\],:{}\s]*$/.test(text.replace(/\\(?:["\\\/bfnrt]|u[0-9a-fA-F]{4})/g, "@").replace(/"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g, "]").replace(/(?:^|:|,)(?:\s*\[)+/g, "")) ? j = eval("(" + text + ")") : (this.php_js = this.php_js || {}, this.php_js.last_error_json = 4, null)
    }
}