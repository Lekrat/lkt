export class StringHelper {

    static kebabCaseToCamelCase(str = ''){
        return str.replace(/-([a-z])/g, function (g) { return g[1].toUpperCase(); });
    }

    static explode(t, e, r) {
        if (arguments.length < 2 || "undefined" == typeof t || "undefined" == typeof e) {
            return null;
        }
        if ("" === t || t === !1 || null === t) {
            return !1;
        }
        if ("function" == typeof t || "object" == typeof t || "function" == typeof e || "object" == typeof e) {
            return {
                0: ""
            };
        }
        t === !0 && (t = "1"), t += "", e += "";
        let n = e.split(t);
        return "undefined" == typeof r ? n : (0 === r && (r = 1), r > 0 ? r >= n.length ? n : n.slice(0, r - 1).concat([n.slice(r - 1).join(t)]) : -r >= n.length ? [] : (n.splice(n.length + r), n))
    }

    static length(t) {
        let e = t + "",
            r = 0,
            n = "",
            o = 0;

        let i = function (t, e) {
            let r = t.charCodeAt(e),
                n = "",
                o = "";
            if (r >= 55296 && 56319 >= r) {
                if (t.length <= e + 1) {
                    throw "High surrogate without following low surrogate";
                }
                if (n = t.charCodeAt(e + 1), 56320 > n || n > 57343) {
                    throw "High surrogate without following low surrogate";
                }
                return t.charAt(e) + t.charAt(e + 1)
            }
            if (r >= 56320 && 57343 >= r) {
                if (0 === e) {
                    throw "Low surrogate without preceding high surrogate";
                }
                if (o = t.charCodeAt(e - 1), 55296 > o || o > 56319) {
                    throw "Low surrogate without preceding high surrogate";
                }
                return !1
            }
            return t.charAt(e)
        };
        for (r = 0, o = 0; r < e.length; r++) (n = i(e, r)) !== !1 && o++;
        return o
    }

    static trim(t, e) {
        let r, n = 0,
            o = 0;
        for (t += "", e ? (e += "", r = e.replace(/([\[\]\(\)\.\?\/\*\{\}\+\$\^\:])/g, "$1")) : r = " \n\r	\f            ​\u2028\u2029　", n = t.length, o = 0; n > o; o++)
            if (-1 === r.indexOf(t.charAt(o))) {
                t = t.substring(o);
                break
            }
        for (n = t.length, o = n - 1; o >= 0; o--)
            if (-1 === r.indexOf(t.charAt(o))) {
                t = t.substring(0, o + 1);
                break
            }
        return -1 === r.indexOf(t.charAt(0)) ? t : ""
    }

    static ucfirst(t) {
        t += "";
        let e = t.charAt(0).toUpperCase();
        return e + t.substr(1)
    }

    static replaceAll(target, search, replacement) {
        return target.replace(new RegExp(search, 'g'), replacement);
    }

    static replaceSingleWhiteSpaces(target, replacement) {
        return target.replace(/\s/g, replacement);
    }

    static stripTags(input, allowed) {
        if (input === null) {
            input = '';
        }
        // making sure the allowed arg is a string containing only tags in lowercase (<a><b><c>)
        allowed = (((allowed || '') + '').toLowerCase().match(/<[a-z][a-z0-9]*>/g) || []).join('')

        let tags = /<\/?([a-z][a-z0-9]*)\b[^>]*>/gi;
        let commentsAndPhpTags = /<!--[\s\S]*?-->|<\?(?:php)?[\s\S]*?\?>/gi;

        return input.replace(commentsAndPhpTags, '').replace(tags, function ($0, $1) {
            return allowed.indexOf('<' + $1.toLowerCase() + '>') > -1 ? $0 : ''
        })
    }

    static numberFormat(t, e, r, n) {
        t = (t + "").replace(/[^0-9+\-Ee.]/g, "");
        let o = isFinite(+t) ? +t : 0,
            i = isFinite(+e) ? Math.abs(e) : 0,
            u = "undefined" === typeof n ? "," : n,
            a = "undefined" === typeof r ? "." : r,
            s = "",
            f = function (t, e) {
                let r = Math.pow(10, e);
                return "" + (Math.round(t * r) / r).toFixed(e)
            };
        return s = (i ? f(o, i) : "" + Math.round(o)).split("."), s[0].length > 3 && (s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, u)), (s[1] || "").length < i && (s[1] = s[1] || "", s[1] += new Array(i - s[1].length + 1).join("0")), s.join(a)
    }

    static utf8Encode (string) {
        string = string.replace(/\r\n/g, "\n");
        let utftext = "";

        for (var n = 0; n < string.length; n++) {

            var c = string.charCodeAt(n);

            if (c < 128) {
                utftext += String.fromCharCode(c);
            }
            else if ((c > 127) && (c < 2048)) {
                utftext += String.fromCharCode((c >> 6) | 192);
                utftext += String.fromCharCode((c & 63) | 128);
            }
            else {
                utftext += String.fromCharCode((c >> 12) | 224);
                utftext += String.fromCharCode(((c >> 6) & 63) | 128);
                utftext += String.fromCharCode((c & 63) | 128);
            }

        }

        return utftext;
    }

    static utf8Decode (utftext) {
        let string = "",
            i = 0,
            c = 0,
            c1 = 0,
            c2 = 0,
            c3 = 0;

        while (i < utftext.length) {

            c = utftext.charCodeAt(i);

            if (c < 128) {
                string += String.fromCharCode(c);
                i++;
            }
            else if ((c > 191) && (c < 224)) {
                c2 = utftext.charCodeAt(i + 1);
                string += String.fromCharCode(((c & 31) << 6) | (c2 & 63));
                i += 2;
            }
            else {
                c2 = utftext.charCodeAt(i + 1);
                c3 = utftext.charCodeAt(i + 2);
                string += String.fromCharCode(((c & 15) << 12) | ((c2 & 63) << 6) | (c3 & 63));
                i += 3;
            }
        }

        return string;
    }

    static extractFillData(str = '', replacements = {}, leftSeparator = ':', rightSeparator = '') {
        let r = [];
        for (let k in replacements) {
            if (replacements.hasOwnProperty(k)) {
                if (str.indexOf(leftSeparator + k + rightSeparator) > -1){
                    r.push(k);
                }
            }
        }
        return r;
    }

    static fill(str = '', replacements = {}, leftSeparator = ':', rightSeparator = '') {
        for (let k in replacements) {
            if (replacements.hasOwnProperty(k)) {
                str = str.replace(leftSeparator + k + rightSeparator, replacements[k]);
            }
        }
        return str;
    }

    static generateRandomString(length = 10) {
        let result = '',
            characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789',
            charactersLength = characters.length;

        for (let i = 0; i < length; i++) {
            result += characters.charAt(Math.floor(Math.random() * charactersLength));
        }

        return result;
    }

    static htmlEntities(str) {
        return String(str).replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;');
    }
}