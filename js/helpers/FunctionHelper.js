import {TypeHelper} from "./TypeHelper";

export const FunctionHelper = {
    clone(that){
        if (TypeHelper.isFunction(that)){
            var temp = function temporary() { return that.apply(this, arguments); };
            for(var key in this) {
                if (this.hasOwnProperty(key)) {
                    temp[key] = this[key];
                }
            }
            return temp;
        }
        return undefined;
    }
};