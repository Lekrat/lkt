import {TypeHelper} from "./TypeHelper";

export class ImageHelper {
    static isBase64(src = ''){
        return TypeHelper.isString(src) && src !== '' && src.indexOf('data:image/') !== -1;
    }
}