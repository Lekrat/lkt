import {TypeHelper} from "../TypeHelper";

export const AlphabeticallyAscendantSorter = (a = undefined, b = undefined, compareProp = undefined) => {
    let A = TypeHelper.isUndefined(compareProp) ? a : a[compareProp],
        B = TypeHelper.isUndefined(compareProp) ? b : b[compareProp];

    if (A < B) {
        return -1;
    }
    if (A > B) {
        return 1;
    }
    return 0;
};