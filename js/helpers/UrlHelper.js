import {StringHelper} from "./StringHelper";

export class UrlHelper {
    static decodeUrl(str) {
        return decodeURIComponent((str + '')
            .replace(/%(?![\da-f]{2})/gi, function () {
                // PHP tolerates poorly formed escape sequences
                return '%25';
            })
            .replace(/\+/g, '%20'));
    }

    static toSlug(str){
        str = str.toLowerCase();
        str = StringHelper.replaceSingleWhiteSpaces(str,'-');
        str = StringHelper.replaceAll(str, 'ñ', 'n');
        str = StringHelper.replaceAll(str, '\'', '');
        str = StringHelper.replaceAll(str, '´', '');
        str = StringHelper.replaceAll(str, '\\', '');
        str = StringHelper.replaceAll(str, '.', '');
        str = StringHelper.replaceAll(str, ',', '');
        str = StringHelper.replaceAll(str, '/', '');
        str = StringHelper.replaceAll(str, ':', '');
        str = StringHelper.replaceAll(str, 'á', 'a');
        str = StringHelper.replaceAll(str, 'é', 'e');
        str = StringHelper.replaceAll(str, 'í', 'i');
        str = StringHelper.replaceAll(str, 'ó', 'o');
        str = StringHelper.replaceAll(str, 'ú', 'u');
        str = StringHelper.replaceAll(str, 'à', 'a');
        str = StringHelper.replaceAll(str, 'à', 'e');
        str = StringHelper.replaceAll(str, 'à', 'i');
        str = StringHelper.replaceAll(str, 'à', 'o');
        str = StringHelper.replaceAll(str, 'à', 'u');
        str = StringHelper.replaceAll(str, 'ẃ', 'w');
        str = StringHelper.replaceAll(str, 'ẁ', 'w');
        str = StringHelper.replaceAll(str, 'ý', 'y');
        str = StringHelper.replaceAll(str, 'ỳ', 'y');
        str = StringHelper.replaceAll(str, 'ś', 's');
        str = StringHelper.replaceAll(str, 'ǵ', 'g');
        str = StringHelper.replaceAll(str, 'ḱ', 'k');
        str = StringHelper.replaceAll(str, 'ĺ', 'l');
        str = StringHelper.replaceAll(str, 'ź', 'z');
        str = StringHelper.replaceAll(str, 'ć', 'c');
        str = StringHelper.replaceAll(str, 'ǘ', 'v');
        str = StringHelper.replaceAll(str, 'ń', 'n');
        str = StringHelper.replaceAll(str, 'ḿ', 'm');
        str = StringHelper.replaceAll(str, 'ǹ', 'n');
        str = StringHelper.replaceAll(str, 'ǜ', 'v');
        str = StringHelper.replaceAll(str, 'ä', 'a');
        str = StringHelper.replaceAll(str, 'ë', 'e');
        str = StringHelper.replaceAll(str, 'ï', 'i');
        str = StringHelper.replaceAll(str, 'ö', 'o');
        str = StringHelper.replaceAll(str, 'ü', 'u');
        str = StringHelper.replaceAll(str, 'ẅ', 'w');
        str = StringHelper.replaceAll(str, 'ÿ', 'y');
        str = StringHelper.replaceAll(str, 'ẍ', 'x');
        return str;
    }
}