import {EnvironmentInstance} from './classes/EnvironmentInstance';

export const Environment = {

    DEFAULT_DATA: {
        name: undefined,
        url: undefined,
        auth: {
            user: undefined,
            password: undefined
        }
    },

    INSTANCES: {},

    instance: EnvironmentInstance,

    create(data = Environment.DEFAULT_DATA){
        let r = new Environment.instance(data);
        Environment.INSTANCES[r.name] = r;
        return Environment;
    },

    get(name = ''){
        return Environment.INSTANCES[name];
    }
};