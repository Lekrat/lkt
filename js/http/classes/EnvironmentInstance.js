export class EnvironmentInstance {
    constructor (data = {}){
        this.name = data.name;
        this.url = data.url;
        this.auth = data.auth;
    }
}