import {ResourceInstance} from "./classes/ResourceInstance";
import {ObjectHelper} from "../helpers/ObjectHelper";

export const Resource = {
    DEFAULT_DATA: {
        name: undefined,
        url: undefined,
        method: undefined,
        environment: undefined,
        unsafeParams: false,
        params: {},
        renameParams: {},
        fillLeftSeparator: '{',
        fillRightSeparator: '}',
        success: undefined,
        validStatuses: [200, 201, 202],
        searchParam: undefined,
        processResults: undefined,
        escapeMarkup: undefined,
        templateResult: undefined,
        templateSelection: undefined,
        paginationVariable: 'paged',
        enableMultipleCalling: false,
        isFileUpload: false,
        lastPageChecker(){
            return false;
        },
        dataType: 'json',
        cache: {},
        cacheTime: 0,
    },

    INSTANCES: {},

    instance: ResourceInstance,

    create(data = Resource.DEFAULT_DATA) {
        let r = new Resource.instance(data);
        Resource.INSTANCES[r.name] = r;
        return Resource;
    },

    createMany(resources = []) {
        resources.forEach(resource => {
            Resource.create(resource);
        });
    },

    get(name = '') {
        return ObjectHelper.clone(Resource.INSTANCES[name]);
    },

    paramsToString(params = {}) {
        let r = [];
        for (let key in params) {
            if (params.hasOwnProperty(key)) {
                if (Array.isArray(params[key])) {
                    if (params[key].length > 0) {
                        r.push(key + '=' + JSON.stringify(params[key]) + '');
                    }
                } else {
                    r.push(key + '=' + params[key]);
                }
            }
        }

        return r.join('&');
    }
};