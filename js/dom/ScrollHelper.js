export const ScrollHelper = {
    getDocumentHeight() {
        let D = document;
        return Math.max(
            D.body.scrollHeight, D.documentElement.scrollHeight,
            D.body.offsetHeight, D.documentElement.offsetHeight,
            D.body.clientHeight, D.documentElement.clientHeight
        );
    },

    getWindowScrollPosition() {
        let supportPageOffset = window.pageXOffset !== undefined,
            isCSS1Compat = ((document.compatMode || "") === "CSS1Compat"),
            x = supportPageOffset ? window.pageXOffset : isCSS1Compat ? document.documentElement.scrollLeft : document.body.scrollLeft,
            y = supportPageOffset ? window.pageYOffset : isCSS1Compat ? document.documentElement.scrollTop : document.body.scrollTop;

        return { x, y };
    },

    getWindowSize(){
        let w = window.innerWidth
            || document.documentElement.clientWidth
            || document.body.clientWidth,

            h = window.innerHeight
                || document.documentElement.clientHeight
                || document.body.clientHeight;

        return {w, h};
    },

    getElementSize(element){
        return { h: element.offsetHeight, w: element.offsetWidth };
    },

    windowScrollAtBottom() {
        return ScrollHelper.getWindowScrollPosition().y + ScrollHelper.getWindowSize().h >= ScrollHelper.getDocumentHeight();
    },

    elementScrollAtBottom(element) {
        return element.scrollTop + ScrollHelper.getElementSize(element).h >= element.scrollHeight;
    }
};