export const ImagePreload = {

    load(images = []){

        return new Promise( (resolve, reject) => {
            let c = 0,
                l = images.length,
                update = () => {
                    ++c;

                    if (c >= l){
                        resolve();
                    }
                };

            images.forEach(image => {
                var img = document.createElement('img');

                img.onload = () => {
                    update(img);
                };
                img.onerror = () => {
                    update(img);
                };
                img.onabort = () => {
                    update(img);
                };

                img.src = image;
                img.style.top = 0;
                img.style.left = 0;
                img.style.height = 0;
                img.style.width = 0;
                img.style.position = 'absolute';
                img.style.overflow = 'hidden';
                img.style.zIndex = -1;

                document.body.append(img);
            })
        });


    }
};