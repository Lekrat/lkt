import {ObjectHelper} from "../../helpers/ObjectHelper";
import {TypeHelper} from "../../helpers/TypeHelper";
import {TimeHelper} from "../../helpers/TimeHelper";

export class ModifiedDataController {

    constructor() {
        this.originalData = '';
        this.data = '';
    }

    parseData(data) {

        data = JSON.parse(JSON.stringify(data));

        let r = {};

        for (let k in data) {
            if (data.hasOwnProperty(k) && data[k] !== null) {

                if (data[k] instanceof Date) {
                    if (!isNaN(data[k].valueOf())) {
                        r[k] = TimeHelper.date('Y-m-d H:i:s', data[k]);
                    }

                } else if (TypeHelper.isObject(data[k])) {
                    r[k] = this.parseData(data[k]);

                } else if (TypeHelper.isArray(data[k])) {
                    let tmp = [];
                    data[k].forEach(z => {
                        tmp.push(this.parseData(z));
                    });
                    r[k] = tmp;

                } else if (TypeHelper.isNumeric(data[k])) {
                    r[k] = String(data[k]);

                } else if (!TypeHelper.isFunction(data[k])) {
                    r[k] = data[k];
                }
            }
        }

        r = ObjectHelper.sort(r);
        return JSON.stringify(r);
    }

    store(data) {
        this.data = this.parseData(data);
        return this;
    }

    reset(data) {
        this.data = this.parseData(data);
        this.originalData = this.parseData(data);
        return this;
    }

    hasModifications() {
        return this.data !== this.originalData;
    }
}