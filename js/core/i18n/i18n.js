import {ObjectHelper} from "../../helpers/ObjectHelper";
import {TypeHelper} from "../../helpers/TypeHelper";
import {StringHelper} from "../../helpers/StringHelper";

export var $i18n = {
    i18n: {},
    debugMode: false,

    get(...args) {
        let key = args[0],
            replacements = args[1];

        if (this.debugMode) {
            return key;
        }

        let r = ObjectHelper.fetch(this.i18n, key);

        if (TypeHelper.isString(r)){
            r = StringHelper.fill(r, replacements, ':', '');
        }

        return r;
    },

    debugEnabled(status = false) {
        this.debugMode = status;
    }
}