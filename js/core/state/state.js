export var $state = {
    state: {},

    set(...args) {
        let key = args[0],
            val = args[1];

        if (args.length === 2) {
            this.state[key] = val;
            return val;
        }

        if (args.length === 1) {
            return this.state[key];
        }

        return undefined;
    },
    get(key) {
        return this.set([key]);
    },
    rm(...args) {
        args.forEach(key => {
            delete this.state[key];
        });
        return true;
    }
}