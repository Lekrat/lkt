export const emptyPromise = (cb) => {
    return new Promise(function (resolve, reject) {
        if (typeof cb === 'function') {
            cb();
        }
        resolve(undefined);
    });
}