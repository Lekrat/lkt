import {StringHelper} from "../../helpers/StringHelper";


export const getRefreshModalKey = (modalName, id) => {
    let r = [
        'refreshModal',
        StringHelper.ucfirst(modalName),
    ];

    if (id > 0) {
        r.push(id);
    }

    return r.join('');
}


export const getModalRefKey = (modal, id) => {
    let r = [];
    if (modal) {
        r.push(modal);
    }
    if (id) {
        r.push(id);
    }
    return r.join('-');
}