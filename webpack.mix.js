let mix = require('laravel-mix');
const { VueLoaderPlugin } = require('vue-loader');
// require('laravel-mix-polyfill');

const path = require('path');

// module.exports = {
//     entry: './lkt-ts/lkt.ts',
//     module: {
//         rules: [
//             {
//                 test: /\.tsx?$/,
//                 use: 'ts-loader',
//                 exclude: /node_modules/,
//             },
//         ],
//     },
//     resolve: {
//         extensions: ['.tsx', '.ts', '.js'],
//     },
//     output: {
//         filename: 'bundle.js',
//         path: path.resolve(__dirname, 'dist'),
//     },
// };

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

// mix.webpackConfig({
//     module: {
//         rules: [
//             {
//                 test: /\.vue$/,
//                 loader: 'vue-loader',
//
//                 /**
//                  * For some reason I don't know why removing exclude does not compile properly
//                  */
//                 exclude: /bower_components/,
//
//                 options: {
//                     loaders: {
//                         js: {
//                             loader: 'babel-loader',
//                             // options: Config.babel()
//                         },
//
//                         /**
//                          *  Here's where you put your extra configs
//                          */
//                         scss: [
//                             { loader: 'vue-style-loader' },
//                             { loader: 'css-loader' },
//                             {
//                                 loader: 'sass-loader',
//                                 options:{
//                                     includePaths: ['node_modules']
//                                 }
//                             }
//                         ]
//                     }
//                 }
//             }
//         ]
//     }
// });

mix.webpackConfig({
    optimization: {
        providedExports: false,
        sideEffects: false,
        usedExports: false
    }
});

mix.options({
    autoprefixer: { remove: false }
});

mix
    .js('apps-src/lkt-debug-info/lkt-debug-info.js', 'apps')
        .vue({
        extractStyles: true,
        globalStyles: false
    })
    .js('apps-src/lkt-notifications/lkt-notifications.js', 'apps')
        .vue({
        extractStyles: true,
        globalStyles: false
    })
    .js('demo/demo-scss/src/js/demo-scss.js', 'demo/demo-scss/assets')
        .vue({
        extractStyles: true,
        globalStyles: false
    })
    .js('lkt/lkt.js', 'dist')
    .ts('lkt-ts/lkt.ts', 'lkt-ts-dist')
    .sass('themes/wp-admin/wp-admin-theme.scss', 'dist')
    .sass('demo/demo-scss/src/scss/demo-scss.scss', 'demo/demo-scss/assets')
;

// mix
//     .js('apps-src/lkt-debug-info/lkt-debug-info.js', 'apps')
//     .js('apps-src/lkt-notifications/lkt-notifications.js', 'apps')
//     .js('lkt/lkt.js', 'dist')
//     .sass('themes/wp-admin/wp-admin-theme.scss', 'dist')
//     .options({processCssUrls: false})
//     .polyfill({
//         enabled: true,
//         useBuiltIns: "usage",
//         targets: {"firefox": "50", "ie": 11}
//     })
// ;
