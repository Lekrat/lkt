


import {ArrayHelper} from "../../js/helpers/ArrayHelper";
import {DocumentHelper} from "../../js/helpers/DocumentHelper";
import {FileHelper} from "../../js/helpers/FileHelper";
import {FunctionHelper} from "../../js/helpers/FunctionHelper";
import {ImageHelper} from "../../js/helpers/ImageHelper";
import {NumberHelper} from "../../js/helpers/NumberHelper";
import {ObjectHelper} from "../../js/helpers/ObjectHelper";
import {SocialShareHelper} from "../../js/helpers/SocialShareHelper";
import {StringHelper} from "../../js/helpers/StringHelper";
import {ScrollHelper} from "../../js/dom/ScrollHelper";
import {TimeHelper} from "../../js/helpers/TimeHelper";
import {UrlHelper} from "../../js/helpers/UrlHelper";
import {TypeHelper} from "../../js/helpers/TypeHelper";
import {NavigatorHelper} from "../../js/helpers/NavigatorHelper";

export function LktHelpers (lkt) {
    lkt.Helper = lkt.Helper || {};

    lkt.Helper.Array = ArrayHelper;
    lkt.Helper.Document = DocumentHelper;
    lkt.Helper.File = FileHelper;
    lkt.Helper.Function = FunctionHelper;
    lkt.Helper.Image = ImageHelper;
    lkt.Helper.Navigator = NavigatorHelper;
    lkt.Helper.Number = NumberHelper;
    lkt.Helper.Object = ObjectHelper;
    lkt.Helper.SocialShare = SocialShareHelper;
    lkt.Helper.String = StringHelper;
    lkt.Helper.Scroll = ScrollHelper;
    lkt.Helper.Time = TimeHelper;
    lkt.Helper.Type = TypeHelper;
    lkt.Helper.Url = UrlHelper;
}