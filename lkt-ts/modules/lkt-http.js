import {Environment} from "../../js/http/Environment";
import {EnvironmentInstance} from "../../js/http/classes/EnvironmentInstance";
import {Resource} from "../../js/http/Resource";
import {ResourceInstance} from "../../js/http/classes/ResourceInstance";

export function LktHttp (lkt) {
    lkt.Http = lkt.Http || {};
    lkt.Http.Environment = Environment;
    lkt.Http.EnvironmentInstance = EnvironmentInstance;
    lkt.Http.Resource = Resource;
    lkt.Http.ResourceInstance = ResourceInstance;
}