import {AlphabeticallyAscendantSorter} from "../../js/helpers/sorters/AlfabeticallyAscendantSorter";
import {AlphabeticallyDescendantSorter} from "../../js/helpers/sorters/AlfabeticallyDescendantSorter";

export function LktSorters (lkt) {
    lkt.Sort = lkt.Sort || {};
    lkt.Sort.AlphabeticallyAscendantSorter = AlphabeticallyAscendantSorter;
    lkt.Sort.AlphabeticallyDescendantSorter = AlphabeticallyDescendantSorter;
}