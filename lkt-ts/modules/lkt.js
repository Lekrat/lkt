import {EmailValidation} from "../../js/helpers/validations/EmailValidation";
import {EmptyValidation} from "../../js/helpers/validations/EmptyValidation";
import {NotEmptyValidation} from "../../js/helpers/validations/NotEmptyValidation";

export function Lkt () {
    this.Helper = {
        Array: undefined,
        Document: undefined,
        File: undefined,
        Function: undefined,
        Image: undefined,
        Navigator: undefined,
        Number: undefined,
        Object: undefined,
        SocialShare: undefined,
        String: undefined,
        Time: undefined,
        Type: undefined,
        Url: undefined,
    };

    this.Http = {
        Environment: undefined,
        EnvironmentInstance: undefined,
        Resource: undefined,
        ResourceInstance: undefined,
    };

    this.Sort = {};

    this.Validations = {
        Email: EmailValidation,
        Empty: EmptyValidation,
        NotEmpty: NotEmptyValidation
    };
}