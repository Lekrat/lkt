// import {Lkt} from "./modules/lkt";
import {LktHelpers} from "./modules/lkt-helpers";
import {LktHttp} from "./modules/lkt-http";
import {EmptyValidation} from "../js/helpers/validations/EmptyValidation";
import {NotEmptyValidation} from "../js/helpers/validations/NotEmptyValidation";
import {EmailValidation} from "../js/helpers/validations/EmailValidation";

class Lkt {
    Helper: object;
    Http: object;
    Sort: object;
    Validations: object;

    constructor() {
        this.Helper = {
            Array: undefined,
            Document: undefined,
            File: undefined,
            Function: undefined,
            Image: undefined,
            Navigator: undefined,
            Number: undefined,
            Object: undefined,
            SocialShare: undefined,
            String: undefined,
            Time: undefined,
            Type: undefined,
            Url: undefined,
        };

        this.Http = {
            Environment: undefined,
            EnvironmentInstance: undefined,
            Resource: undefined,
            ResourceInstance: undefined,
        };

        this.Sort = {};

        this.Validations = {
            Email: EmailValidation,
            Empty: EmptyValidation,
            NotEmpty: NotEmptyValidation
        };
    }


}

let _lkt = new Lkt();

LktHelpers(_lkt);
LktHttp(_lkt);

export const lkt = _lkt;