# Installation

Execute the next command:

```
npm i git+https://gitlab.com/Lekrat/lkt.git --save-dev
```

# Known issues
- After ```npm update``` it's necessary to run ```npm install``` (this package is being uninstalled).

# Usage

## SCSS

```
@import "~lkt";
```