import {OptionFormatter} from "../../classes/lkt-field/OptionFormatter";

export const DefaultOptionFormatter = new OptionFormatter((option = {}) => {
    return option.label;

});