import {OptionFormatter} from "../../classes/lkt-field/OptionFormatter";

export const OptionWithImageFormatter = new OptionFormatter((option = {}) => {
    return '<img src="'+option.image+'" /><span>' + option.label + '</span>';
});