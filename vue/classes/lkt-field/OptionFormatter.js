import {TypeHelper} from "../../../js/helpers/TypeHelper";

export class OptionFormatter {

    constructor(handler) {
        if (!TypeHelper.isFunction(handler)) {
            handler = () => {
            };
        }
        this.handler = handler;
    }

    handle(option) {
        return this.handler(option);
    }
}
