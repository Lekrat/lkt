export const DataFillMixin = {
    methods: {
        fillData(data = {}) {
            for (let k in data) {
                if (data.hasOwnProperty(k)) {
                    this[k] = data[k];
                }
            }
            return this;
        }
    }
};