// import {LktMixin} from "./LktMixin";
import {ResponsiveCommon} from "./responsive/ResponsiveCommon";
import {StateCommon} from "./state/StateCommon";
import {ApiMixin} from "./http/ApiMixin";
import {NotificationsCommonMixin} from "./notifications/NotificationsCommonMixin";
import {I18nCommonMixin} from "./i18n/I18nCommonMixin";
import {ThemeCommonMixin} from "./theme/ThemeCommonMixin";
import {ModalControllerCommonMixin} from "./modals/ModalControllerCommonMixin";

export const MetaCommonMixin = {
    mixins: [ApiMixin, ResponsiveCommon, StateCommon, NotificationsCommonMixin, I18nCommonMixin, ThemeCommonMixin, ModalControllerCommonMixin],
};