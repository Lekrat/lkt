export const I18nRootMixin = {
    data() {
        return {
            I18N: {}
        };
    },
    methods: {
        setI18n(data) {
            this.$i18n.i18n = data;
        }
    }
};