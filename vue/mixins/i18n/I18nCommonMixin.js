export const I18nCommonMixin = {
    methods: {
        '$__'(key = '', replacements = {}) {
            return this.$i18n.get(key, replacements);
        }
    }
};