export const ApiMixin = {
    methods: {
        '$api'(resourceName = '', params =  {}, options = {}){
            let resource = this.$lkt.Http.Resource.get(resourceName);

            if (!options.forceRefresh && this.MODAL_REFRESHING){
                options.forceRefresh = this.MODAL_REFRESHING;
            }

            if (options.forceRefresh === true){
                resource.forceRefresh();
            }

            return resource.call(params);
        }
    }
}