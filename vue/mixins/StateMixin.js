export const StateMixin = {
    props: {
        state: {
            type: String,
            default: '',
        }
    }
};