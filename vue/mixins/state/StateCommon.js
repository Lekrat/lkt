export const StateCommon = {
    computed: {
        // Vue 2 doesn't allow to watch a computed variables in computed shorthand
        // must declare watchers with this.$watch
        // '$state'() {
        //     return this.$root.STATE;
        // },
    },
    methods: {
        '$_state'(...args) {
            let key = args[0],
                val = args[1];

            if (args.length === 2) {
                this.$root.$set(this.$state, key, val);
                return val;
            }

            if (args.length === 1) {
                return this.$state[key];
            }

            return undefined;
        },
        '$_unstate'(...args) {
            args.forEach(key => {
                delete this.$state[key];
            });
            return true;
        }
    },
};
