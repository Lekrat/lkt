import LktNotifications from "../../components/LktNotifications";

export const NotificationsRootMixin = {
    methods: {
        setNotificationsRoot(){
            if (!this.$refs.lktNotifications){
                let ComponentClass = Vue.extend(LktNotifications),
                    instance = new ComponentClass();
                instance.$root = this.$root;
                instance.$parent = this;
                instance.$mount();
                this.$children.push(instance);
                this.$el.append(instance.$el);
                this.$refs.lktNotifications = instance;
            }
        }
    }
};