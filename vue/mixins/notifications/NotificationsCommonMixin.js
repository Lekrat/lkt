export const NotificationsCommonMixin = {
    methods: {
        $notify(notification) {
            this.$root.$refs.lktNotifications.push(notification);
        },
    }
};