export const Helper = {
    methods: {
        fillString: function (str, replacements) {
            for (var k in replacements) {
                if (replacements.hasOwnProperty(k)) {
                    str = str.replace(':' + k, replacements[k]);
                }
            }
            return str;
        },
        scrollTop: function () {
            if (!window.scrollTopTimeout) {
                window.scrollTopTimeout = undefined;
            }
            if (document.body.scrollTop !== 0 || document.documentElement.scrollTop !== 0) {
                document.body.scrollBy(0, -50);
                window.scrollTopTimeout = setTimeout(this.scrollTop, 10);
            } else {
                clearTimeout(window.scrollTopTimeout);
            }
        },
    },
};
