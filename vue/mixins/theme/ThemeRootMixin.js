export const ThemeRootMixin = {
    data(){
        return {
            THEME: ''
        };
    },
    methods: {
        setRootTheme(theme = ''){
            this.THEME = theme;
        }
    }
};