export const ThemeCommonMixin = {
    props: {
        theme: {
            type: String,
            default: '',
        },
        noTheme: {
            type: Boolean,
            default: false,
        }
    },
    computed: {
        '$theme'(){
            if (this.noTheme){
                return '';
            }
            if (this.theme){
                return this.theme;
            }
            return this.$root.THEME;
        }
    }
};