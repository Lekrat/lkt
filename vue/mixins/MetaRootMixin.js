import {ResponsiveRoot} from "./responsive/ResponsiveRoot";
import {StateRoot} from "./state/StateRoot";
import {NotificationsRootMixin} from "./notifications/NotificationsRootMixin";
import {I18nRootMixin} from "./i18n/I18nRootMixin";
import {ThemeRootMixin} from "./theme/ThemeRootMixin";
import {ModalControllerRootMixin} from "./modals/ModalControllerRootMixin";

export const MetaRootMixin = {
    mixins: [ResponsiveRoot, StateRoot, NotificationsRootMixin, I18nRootMixin, ThemeRootMixin, ModalControllerRootMixin],
    methods: {
        setMetaRootMixin() {
            // Trigger responsive
            this.onResizeResponsiveRoot();

            // Start mixins
            this.setResizeResponsiveRoot();
            this.setNotificationsRoot();
            this.setModalControllerRoot();
        },
        unsetMetaRootMixin() {
            this.unsetResizeResponsiveRoot();
        },
    },
    mounted() {
        this.setMetaRootMixin();
    },
    beforeDestroy() {
        this.unsetMetaRootMixin();
    },
};