import LktModalController from "../../components/LktModalController";

export const ModalControllerRootMixin = {
    data() {
        return {
            MODALS: {},
        };
    },
    methods: {
        setModalControllerRoot() {
            if (!this.$refs.lktModalController) {
                let ComponentClass = Vue.extend(LktModalController),
                    instance = new ComponentClass({
                        propsData: {modals: this.MODALS},
                    });
                instance.$root = this.$root;
                instance.$parent = this;
                instance.$mount();
                this.$children.push(instance);
                this.$el.append(instance.$el);
                this.$refs.lktModalController = instance;
            }
        }
    }
};