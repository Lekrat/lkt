import {TypeHelper} from "../../../js/helpers/TypeHelper";

export const ModalControllerMixin = {
    data() {
        return {
            isOpen: false,
            isLoading: false,
        }
    },
    watch: {
        isOpen: {
            handler(val) {
                if (val) {
                    if (TypeHelper.isFunction(this['onOpen'])) {
                        this['onOpen']();
                    }
                    this.$emit('open');
                } else {
                    if (TypeHelper.isFunction(this['onClose'])) {
                        this['onClose']();
                    }
                    this.$emit('close');
                }
            }
        }
    },
    methods: {
        isOpened() {
            return this.isOpen;
        },
        toggle(status = undefined) {
            if (typeof status === 'undefined') {
                status = !this.isOpen;
            }
            this.isOpen = status;
            return status;
        },
        toggleLoader(status = undefined) {
            if (typeof status === 'undefined') {
                status = !this.isLoading;
            }
            this.isLoading = status;
            return status;
        },
    }
};