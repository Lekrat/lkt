import {ModifiedDataMixin} from "../ModifiedDataMixin";
import {getRefreshModalKey} from "../../../js/core/functions/modal-keys";
import {emptyPromise} from "../../../js/core/functions/empty-promise";

export const LktModalInstanceMixin = {
    mixins: [ModifiedDataMixin],
    data() {
        return {
            MODAL_OPEN: false,
            MODAL_LOADING: false,
            MODAL_LOADING_ON_SAVE: true,
            MODAL_REFRESHING: false,
            MODAL_NAME: '',
            MODAL_ID: '',
            MODAL_ORIGINAL_ID: '',
            MODAL_REFRESH_WATCHER: undefined,
            MODAL_CALL_DATA: undefined,
            MODAL_READY: false,
            MODAL_ACTIONS: {

                // Available actions: show, hide, save
                show: {
                    handler: undefined, // Handler should return a promise
                    moment: 0, // 0: Default. Like a callback; 1: Before save;
                },

                hide: {
                    handler: undefined, // Handler should return a promise
                    moment: 0, // 0: Default. Like a callback; 1: Before hide
                },

                drop: {
                    handler: undefined, // Handler should return a promise
                    moment: 0, // 0: Default. Like a callback; 1: Before drop
                },

                save: {
                    handler: undefined, // Handler should return a promise
                    moment: 0, // 0: Default. Like a callback; 1: Before save; 2: Only callback (normal actions won't be executed)
                }
            },
        };
    },
    computed: {
        hasShowAction() {
            return this.MODAL_ACTIONS
                && this.MODAL_ACTIONS.show
                && this.$root.$lkt.Helper.Type.isFunction(this.MODAL_ACTIONS.show.handler);
        },
        momentShowAction() {
            if (this.hasShowAction) {
                return parseInt(this.MODAL_ACTIONS.show.moment);
            }
            return 0;
        },
        hasSaveAction() {
            return this.MODAL_ACTIONS
                && this.MODAL_ACTIONS.save
                && this.$root.$lkt.Helper.Type.isFunction(this.MODAL_ACTIONS.save.handler);
        },
        momentSaveAction() {
            if (this.hasSaveAction) {
                return parseInt(this.MODAL_ACTIONS.save.moment);
            }
            return 0;
        },
        hasDropAction() {
            return this.MODAL_ACTIONS
                && this.MODAL_ACTIONS.drop
                && this.$root.$lkt.Helper.Type.isFunction(this.MODAL_ACTIONS.drop.handler);
        },
        momentDropAction() {
            if (this.hasSaveAction) {
                return parseInt(this.MODAL_ACTIONS.drop.moment);
            }
            return 0;
        }
    },
    watch: {
        MODAL_LOADING(value) {
            if (!value) {
                setTimeout(() => {
                    this.MODAL_REFRESHING = false;
                }, 250)
            }
        }
    },
    methods: {
        reset() {
        },
        beforeHide() {
            return true;
        },
        beforeSave() {
            return true;
        },
        doShow(data = {}) {
            this.MODAL_LOADING = true;

            this.MODAL_CALL_DATA = data;

            // Stop watcher
            if (this.MODAL_REFRESH_WATCHER) {
                this.MODAL_REFRESH_WATCHER();
            }

            // Initialize single modal refresh watcher
            let refreshWatcherKey = '$state.' + getRefreshModalKey(this.MODAL_NAME, this.MODAL_ID);

            this.MODAL_REFRESH_WATCHER = this.$watch(refreshWatcherKey, () => {
                this.doRefresh();
            });

            let moment0 = undefined,
                moment1 = undefined;

            let show = () => {
                // Call reset method (if any)
                if (this.$lkt.Helper.Type.isFunction(this.reset)) {
                    this.reset(data);
                }

                this.MODAL_OPEN = true;
                if (this.$lkt.Helper.Type.isFunction(this.show)) {
                    this.show(data);
                }

                let promise = undefined;

                // Load data
                if (this.$lkt.Helper.Type.isFunction(this.load)) {
                    let closeModal = () => {
                        this.MODAL_OPEN = false;
                        this.$modal({modal: this.MODAL_NAME, id: this.MODAL_ID}, 2);
                    };

                    promise = this.load(data);

                    if (promise) {
                        promise.catch(closeModal);
                    } else {
                        promise = emptyPromise();
                    }
                }

                this.MODAL_READY = true;
                return promise;
            }

            if (this.momentShowAction === 1) {
                if (this.hasShowAction) {
                    moment0 = this.MODAL_ACTIONS.show.handler;
                }
                moment1 = show;

            } else {
                moment0 = show;
                if (this.hasShowAction) {
                    moment1 = this.MODAL_ACTIONS.show.handler;
                }
            }

            return this.doAction(moment0, moment1);
        },
        doHide(data = {}) {
            let check = true;
            if (this.$lkt.Helper.Type.isFunction(this.beforeHide)) {
                check = this.beforeHide(data);
            }
            if (check !== false) {
                this.MODAL_OPEN = false;
                // if (this.MODAL_ID !== this.MODAL_ORIGINAL_ID) {
                //     this.$modal({modal: this.MODAL_NAME, id: this.MODAL_ORIGINAL_ID}, 2);
                // } else {
                    this.$modal({modal: this.MODAL_NAME, id: this.MODAL_ID}, 2);
                // }
            }
        },
        doSave() {
            if (this.MODAL_LOADING_ON_SAVE) {
                this.MODAL_LOADING = true;
            }
            let check = true;
            if (this.$lkt.Helper.Type.isFunction(this.beforeSave)) {
                check = this.beforeSave();
            }

            if (check === false) {
                return;
            }

            let moment0 = undefined,
                moment1 = undefined;

            let save = () => {
                if (this.$lkt.Helper.Type.isFunction(this.save)) {
                    return this.save();
                }
            };

            if (this.momentSaveAction === 1) {
                if (this.hasSaveAction) {
                    moment0 = this.MODAL_ACTIONS.save.handler;
                }
                moment1 = save;

            } else if (this.momentSaveAction === 0) {
                moment0 = save;
                if (this.hasSaveAction) {
                    moment1 = this.MODAL_ACTIONS.save.handler;
                }
            } else {
                if (this.hasSaveAction) {
                    moment0 = this.MODAL_ACTIONS.save.handler;
                }
            }

            return this.doAction(moment0, moment1);
        },
        doDrop() {
            let moment0 = undefined,
                moment1 = undefined;

            let drop = () => {
                if (this.$lkt.Helper.Type.isFunction(this.drop)) {
                    return this.drop();
                }
            };

            if (this.momentDropAction === 1) {
                if (this.hasDropAction) {
                    moment0 = this.MODAL_ACTIONS.drop.handler;
                }
                moment1 = drop;

            } else if (this.momentDropAction === 0) {
                moment0 = drop;
                if (this.hasDropAction) {
                    moment1 = this.MODAL_ACTIONS.drop.handler;
                }
            }

            return this.doAction(moment0, moment1);
        },
        doRefresh2(id = undefined) {
            if (typeof id === 'undefined') {
                this.MODAL_REFRESHING = true;
                return this.doShow(this.MODAL_CALL_DATA);
            }
            return this.doUpdateId(id);
        },
        doRefresh(id = undefined) {
            let data = {};
            if (this.$lkt.Helper.Type.isObject(this.MODAL_CALL_DATA)) {
                data = this.$lkt.Helper.Object.clone(this.MODAL_CALL_DATA);
            }
            if (typeof id !== 'undefined') {
                this.$updateModalRef(this.MODAL_NAME, this.MODAL_NAME, this.MODAL_ID, id);
                data.id = id;
                this.MODAL_ID = id;
            }
            this.MODAL_REFRESHING = true;
            return this.doShow(data);
        },
        doUpdateId(id) {
            let data = {};
            if (this.$lkt.Helper.Type.isObject(this.MODAL_CALL_DATA)) {
                data = this.$lkt.Helper.Object.clone(this.MODAL_CALL_DATA);
            }
            data.id = id;
            return this.$modal({modal: this.MODAL_NAME, id: id, data}, 1).then(() => {
                return this.$modal({modal: this.MODAL_NAME, id: this.MODAL_ID}, 2);
            });
        },
        doAction(moment0, moment1) {

            let finish = () => {
                if (this.MODAL_ID) {
                    setTimeout(() => {
                        this.MODAL_LOADING = false;
                    }, 250);
                } else {
                    this.MODAL_LOADING = false;
                }
            };

            // finish = Promise

            let promise = moment0();

            if (promise && moment1) {
                return promise.then(moment1).then(finish).then(() => {
                    return emptyPromise(finish);
                    // return new Promise(function(resolve, reject) {
                    //     finish();
                    //     resolve();
                    // });
                });
            } else if (promise && !moment1) {
                return promise.then(() => {
                    return emptyPromise(finish);
                    // return new Promise(function(resolve, reject) {
                    //     finish();
                    //     resolve();
                    // });
                });
            } else {
                return emptyPromise(finish);
                // return new Promise(function(resolve, reject) {
                //     finish();
                //     resolve();
                // });
            }
        }
    }
};