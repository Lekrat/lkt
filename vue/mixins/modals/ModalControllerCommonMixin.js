const LKT_MODAL_DEFAULT_ARGS = {
    modal: '',
    id: '',
    data: {},
    actions: {},
};

const LKT_MODAL_CALLS = {
    TOGGLE: 0,
    SHOW: 1,
    HIDE: 2,
};

export const ModalControllerCommonMixin = {
    methods: {
        '$modal'(args = LKT_MODAL_DEFAULT_ARGS, call = LKT_MODAL_CALLS.TOGGLE) {
            return this.$root.$refs.lktModalController.call(args, call);
        },
        '$updateModalRef'(originalName, currentName, originalId, currentId) {
            return this.$root.$refs.lktModalController.updateRef(originalName, currentName, originalId, currentId);
        },
    }
};