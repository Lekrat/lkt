export const FieldStateUIMixin = {

    props: {
        showAdd: {
            type: Boolean,
            default: false,
        },
        showLink: {
            type: Boolean,
            default: false,
        },
        showOpen: {
            type: Boolean,
            default: false,
        },
        showInfo: {
            type: Boolean,
            default: false,
        },
        showPassword: {
            type: Boolean,
            default: false,
        },
        showMandatory: {
            type: Boolean,
            default: false,
        },
        showError: {
            type: Boolean,
            default: false,
        },
        showWarn: {
            type: Boolean,
            default: false,
        },
        showLog: {
            type: Boolean,
            default: false,
        },
        showReset: {
            type: Boolean,
            default: false,
        },
        textAdd: {
            type: String,
            default: '',
        },
        textInfo: {
            type: String,
            default: 'More info',
        },
        textPassword: {
            type: String,
            default: 'Show password',
        },
        textMandatory: {
            type: String,
            default: 'This is mandatory',
        },
        textError: {
            type: String,
            default: '',
        },
        textWarn: {
            type: String,
            default: '',
        },
        textLog: {
            type: String,
            default: '',
        },
        textLink: {
            type: String,
            default: 'Follow link',
        },
        textOpen: {
            type: String,
            default: 'Show details',
        },
        textReset: {
            type: String,
            default: 'Reset',
        },
    },

    computed: {
        showInfoUi(){
            return this.showAdd
                || this.showMandatory
                || this.showOpen
                || this.showInfo
                || this.showError
                || this.showLog
                || this.showWarn
                || this.showLink
                || this.showReset
                || this.showPassword
                ;
        }
    },

    data(){
        return {
        }
    },

    methods: {
        /**
         * This method is to emit the event to the field
         * @param key
         * @param data
         */
        onUIStatusClick(key, data = {}){
            this.$emit('click-ui', key, data);
        },
        /**
         * This method is to emit the event outside the field
         * @param key
         */
        onClickUi(key){
            if (key === 'reset') {
                this.reset();
                return;
            }
            if (key === 'undo') {
                this.undo();
                return;
            }
            if (key === 'show-password' && this.hasOwnProperty('visiblePassword')) {
                this.visiblePassword = !this.visiblePassword;
                return;
            }
            this.onUIStatusClick(key, {
                field: this,
            });
        }
    }
};