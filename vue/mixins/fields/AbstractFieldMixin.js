import {FieldLabelMixin} from "./FieldLabelMixin";
import {FieldStateMixin} from "./FieldStateMixin";
import {FieldNameMixin} from "./FieldNameMixin";
import {LktMixin} from "./../LktMixin";
import {StateMixin} from "./../StateMixin";

export const AbstractFieldMixin = {
    mixins: [FieldLabelMixin, FieldStateMixin, StateMixin, FieldNameMixin, LktMixin],
    props: {
        placeholder: {
            type: String,
            default: '',
        },
    },
    data() {
        return {
            Value: undefined,
            Identifier: '',
            canEmit: false,
        }
    },
    methods: {
        reset() {
            this.Value = undefined;
        },
        getValue(){
            return this.Value;
        }
    },
    watch: {
        value(v) {
            this.Value = v;
        },
        Value(v) {
            this.validate(v);
            if (this.canEmit === true) {
                this.$emit('input', v);
                this.$emit('change', v);
            }
        }
    },
    created() {
        if (this.loadOptions && this.$lkt.Helper.Type.isFunction(this.loadOptions)){
            this.loadOptions(this.options);
        }
        this.Value = this.value;
        this.Identifier = this.$lkt.Helper.String.generateRandomString(16);
        this.setName();
    },
    mounted() {
        this.canEmit = true;
    }
};