export const FieldLabelMixin = {
    props: {
        label: {
            type: String,
            default: '',
        },
    },
    computed: {
        canRenderLabelSlot() {
            return !!this.$slots['label'];
        },
        canRenderLabelHtml() {
            return !!this.label && !this.canRenderLabelSlot;
        }
    }
};