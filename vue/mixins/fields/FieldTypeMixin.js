export const FieldTypeMixin = {
    props: {
        type: String,
        default: 'text',
        validator: function (value) {
            return [
                'text',
                'textarea',
                'number',
                'tel',
                'select',
                'radio',
                'checkbox',
                'password',
                'email',
                'switch',
                'url',
                'date',
                'range',
                'file',
                'image',
                'timepicker'
            ].indexOf(value) !== -1;
        }
    },
    computed: {
        isTypeText(){
            return this.type === 'text';
        },
        isTypeTextArea(){
            return this.type === 'textarea';
        },
        isTypeNumber(){
            return this.type === 'number';
        },
        isTypeTel(){
            return this.type === 'tel';
        },
        isTypeUrl(){
            return this.type === 'url';
        },
        isTypePassword(){
            return this.type === 'password';
        },
        isTypeEmail(){
            return this.type === 'email';
        },
        isTypeSwitch(){
            return this.type === 'switch';
        },
        isTypeRange() {
            return this.type === 'range';
        },
        isTypeSelect() {
            return this.type === 'select';
        },
        isTypeRadio() {
            return this.type === 'radio';
        },
        isTypeCheckBox() {
            return this.type === 'checkbox';
        },
        isTypeDate() {
            return this.type === 'date';
        },
        isTypeFile() {
            return this.type === 'file';
        },
        isTypeImage() {
            return this.type === 'image';
        },
        isTypeTimePicker(){
            return this.type === 'timepicker';
        },
        renderType(){
            switch (this.type) {
                case 'switch':
                    return 'checkbox';
                case 'url':
                case 'select':
                case 'date':
                    return 'text';
                case 'image':
                    return 'file';
                default:
                    return this.type;
            }
        }
    }
};