export const FieldIntervalMixin = {
    props: {
        min: {
            type: Number,
            default: undefined
        },
        max: {
            type: Number,
            default: undefined
        },
        step: {
            type: Number,
            default: 1
        },
    }
};