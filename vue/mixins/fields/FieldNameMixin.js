import {LktMixin} from "./../LktMixin";

export const FieldNameMixin = {
    mixins: [LktMixin],
    props: {
        name: {
            type: String,
            default: '',
        }
    },
    data(){
        return {
            Name: ''
        };
    },
    methods: {
        setName(){
            if (this.name !== ''){
                this.Name = this.name;
            } else if(this.Identifier !== '') {
                this.Name = this.Identifier;
            } else {
                this.Name = this.$lkt.Helper.String.generateRandomString(16);
            }
        }
    }
};