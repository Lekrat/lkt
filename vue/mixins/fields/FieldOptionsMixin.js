import {DefaultOptionFormatter} from "../../helpers/option-formatters/DefaultOptionFormatter";
import {DefaultOptionParser} from "../../helpers/option-parsers/DefaultOptionParser";

export const FieldOptionsMixin = {
    props: {
        options: {
            type: Array,
            default() {
                return [];
            }
        },
        multiple: {
            type: Boolean,
            default: false,
        },
        canTag: {
            type: Boolean,
            default: false,
        },
        noOptionsMessage: {
            type: String,
            default: 'No results. Try typing some text.',
        },
        optionFormatter: {
            type: Object,
            default(){
                return DefaultOptionFormatter;
            }
        },
        optionParser: {
            type: Object,
            default(){
                return DefaultOptionParser;
            }
        }
    },

    data(){
        return {
            Options: [],
        }
    },

    computed: {
        renderSelectedOption: {
            cache: false,
            get(){
                let option = this.Options.filter(opt => {
                    return opt.selected === true;
                });
                return option && option.length > 0 && this.optionFormatter && this.optionFormatter.handle ? this.optionFormatter.handle(option[0]) : this.fetchString;
            }
        }
    },

    methods: {
        getDropdownOptionSelector(option, highlightOption, i){
            let r = {'is-highlight': highlightOption == i, 'is-selected': option.selected === true};
            if (option.selector){
                let temp = option.selector.split(' ');
                temp.forEach(t => {
                    r[t] = true;
                });
            }
            return r;
        },

        renderOption(option){
            return this.optionFormatter && this.optionFormatter.handle ? this.optionFormatter.handle(option) : option.label;
        },

        loadOptions(options = []){
            // console.log('loadOptions');
            let i = 0;
            let opts = options.map((option, i) => {
                return this.optionParser.handle(option, i);
            });

            // opts.every(z => {
            //     z.index = i;
            //     ++i;
            //
            //     z.children.every(y => {
            //         y.index = i;
            //         ++i;
            //     })
            // })

            this.Options = opts;
        }
    }
};