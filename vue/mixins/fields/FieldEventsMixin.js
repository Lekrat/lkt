export const FieldEventsMixin = {
    props: {
        changeDelay: {
            type: Number,
            default: 200,
        },
    }
};