export const FieldStateMixin = {

    props: {
        disabled: {
            type: Boolean,
            default: false,
        },
        readonly: {
            type: Boolean,
            default: false,
        },
        canReset: {
            type: Boolean,
            default: false,
        },
        validation: {
            type: Array,
            default() {
                return [];
            }
        },
    },

    data(){
        return {
            stateChanged: false,
            stateValid: true,
        }
    },

    methods: {

        isChanged(){
            return this.stateChanged;
        },

        isDisabled(isDisabled = undefined) {
            if (typeof isDisabled === 'undefined') {
                return this.disabled;
            } else {
                this.disabled = isDisabled === true;
                return this.disabled;
            }
        },

        isValid(){
            return this.stateValid;
        },

        validate(value = undefined) {
            if (typeof value === 'undefined' && this.$lkt.Helper.Type.isFunction(this.getValue)){
                value = this.getValue();
            }
            let state = true;
            if (this.validation.length > 0) {
                let r = true;
                this.validation.forEach(validator => {
                    r = r && validator.validate(value);
                });
                state = r;
            }
            this.$nextTick(() => {
                this.stateValid = state;
            })
            return this.stateValid;
        },
    }
};