import {lkt} from "./../../lkt/lkt";

export var LktMixin = {
    computed: {
        /**
         *
         * @returns {Lkt}
         */
        '$lkt'() {
            return lkt;
        }
    }
};