import {ModifiedDataController} from "../../js/core/controllers/ModifiedDataController";

export const ModifiedDataMixin = {
    data() {
        return {
            modifiedDataController: new ModifiedDataController(),
            hasModifiedData: false,
            // modifiedDataForm: {},
            // modifiedDataOriginal: {},
            // modifiedDataFirstTime: true,
        }
    },
    watch: {
        // modifiedDataForm: {
        //     handler() {
        //         if (this.modifiedDataFirstTime === true) {
        //             this.modifiedDataFirstTime = false;
        //             return;
        //         }
        //         this.hasModifiedData = this.modifiedDataForm !== this.modifiedDataOriginal;
        //     },
        //     deep: true
        // },
        'modifiedDataController.data'(v) {
            this.hasModifiedData = this.modifiedDataController.hasModifications();
        },
        hasModifiedData(v) {
            this.$emit('has-to-save', v);
        },
    },
    methods: {
        store(data = {}, reset = false) {

            // data = this.$lkt.Helper.Object.sort(data);

            if (reset === true) {
                // this.modifiedDataForm = {};
                // this.modifiedDataFirstTime = true;
                this.modifiedDataController.reset(data);
            } else {
                this.modifiedDataController.store(data);
            }
            // this.hasModifiedData = this.modifiedDataController.hasModifications();
            // if (this.modifiedDataFirstTime === true) {
            //     this.modifiedDataOriginal = JSON.stringify(data);
            // }
            // this.modifiedDataForm = JSON.stringify(data);
            // this.hasModifiedData = this.modifiedDataForm !== this.modifiedDataOriginal;
        }
    }
};
