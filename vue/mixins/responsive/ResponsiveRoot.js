export const ResponsiveRoot = {
    data() {
        return {
            CLIENT_WIDTH: 0,
            CLIENT_HEIGHT: 0,
            WINDOW_WIDTH: 0,
            WINDOW_HEIGHT: 0,
        };
    },
    methods: {
        onResizeResponsiveRoot() {
            this.CLIENT_WIDTH = document.documentElement.clientWidth;
            this.CLIENT_HEIGHT = document.documentElement.clientHeight;
            this.WINDOW_WIDTH = window.innerWidth;
            this.WINDOW_HEIGHT = window.innerHeight;
        },
        setResizeResponsiveRoot() {
            this.$nextTick(() => {
                window.addEventListener('resize', this.onResizeResponsiveRoot);
            })
        },
        unsetResizeResponsiveRoot() {
            window.removeEventListener('resize', this.onResizeResponsiveRoot);
        },
    },

    mounted() {
        this.onResizeResponsiveRoot();
        this.setResizeResponsiveRoot();
    },

    beforeDestroy() {
        this.unsetResizeResponsiveRoot();
    },
};
