export const ResponsiveCommon = {
    computed: {
        '$windowWidth'() {
            return this.$root.WINDOW_WIDTH;
        },
        '$windowHeight'() {
            return this.$root.WINDOW_HEIGHT;
        },
        '$clientWidth'() {
            return this.$root.CLIENT_WIDTH;
        },
        '$clientHeight'() {
            return this.$root.CLIENT_HEIGHT;
        },
    },
};