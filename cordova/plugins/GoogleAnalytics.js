export const GoogleAnalytics = {
    hasGaPlugin (){
        return window.ga && window.ga;
    },
    hasFirebasePlugin (){
        return cordova && cordova.plugins && cordova.plugins.firebase && cordova.plugins.firebase.analytics;
    },
    hasAnalyticsPlugin () {
        return this.hasFirebasePlugin() || this.hasGaPlugin();
    },

    setUserId(userId) {
        if (this.hasFirebasePlugin()){
            cordova.plugins.firebase.analytics.setUserId(userId);

        } else if (this.hasGaPlugin()){
            window.ga.startTrackerWithId(userId);

        } else {
            // console.warn('[Lkt.Cordova.GoogleAnalytics.setUserId] No Analytics plugin detected');
        }
    },

    setScreen(screenName) {
        if (this.hasFirebasePlugin()) {
            cordova.plugins.firebase.analytics.setCurrentScreen(screenName);

        } else if (this.hasGaPlugin()){
            window.ga.trackView(screenName);

        } else {
            // console.warn('[Lkt.Cordova.GoogleAnalytics.setScreen] No Analytics plugin detected');
        }
    }
};