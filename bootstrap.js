/**
 * Require lodash tools
 */
// import _ from 'lodash';

/**
 * Require axios
 */
import axios from 'axios';
axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

/**
 * Require jQuery
 */
import $ from 'jquery';

/**
 * Require vue
 */
import Vue from 'vue';

/**
 * Require VueRouter
 */
import VueRouter from 'vue-router';
Vue.use(VueRouter);

/**
 * Require LKT tools and turn it global
 */
import {lkt} from "./lkt/lkt";
Vue.prototype.$lkt = lkt;

/**
 * Initialize global $state variable
 */
import {$state} from "./js/core/state/state";

const state = Vue.observable($state);
Object.defineProperty(Vue.prototype, '$state', {
    get() { return state.state; },
    set(value) { state.state = value; }
});

/**
 * Initialize global $i18n variable
 */
import {$i18n} from "./js/core/i18n/i18n";

const i18n = Vue.observable($i18n);
let fn = {
    install() {
        Vue.$i18n = i18n;
        Vue.prototype.$i18n = i18n; //this.$gPluginFun()
    }
}

Vue.use(fn);

import vSelect from 'vue-select'

Vue.component('v-select', vSelect)

/**
 * Automatically include all vue-lkt components
 */
import LktAccordion from "./vue/components/LktAccordion";
import LktAnchor from "./vue/components/LktAnchor";
import LktButton from "./vue/components/LktButton";
import LktConfirm from "./vue/components/LktConfirm";
import LktDisableWebkitAutoFill from "./vue/components/LktDisableWebkitAutoFill";
import LktDot from "./vue/components/LktDot";
import LktModal from "./vue/components/LktModal";
import LktWindow from "./vue/components/LktWindow";
import LktNoData from "./vue/components/LktNoData";
import LktPaginator from "./vue/components/LktPaginator";
import LktTab from "./vue/components/LktTab";
import LktTabs from "./vue/components/LktTabs";
import LktTable from "./vue/components/LktTable";

Vue.component('lkt-accordion', LktAccordion);
Vue.component('lkt-anchor', LktAnchor);
Vue.component('lkt-button', LktButton);
Vue.component('lkt-confirm', LktConfirm);
Vue.component('lkt-disable-webkit-auto-fill', LktDisableWebkitAutoFill);
Vue.component('lkt-dot', LktDot);
Vue.component('lkt-modal', LktModal);
Vue.component('lkt-window', LktWindow);
Vue.component('lkt-no-data', LktNoData);
Vue.component('lkt-paginator', LktPaginator);
Vue.component('lkt-tab', LktTab);
Vue.component('lkt-tabs', LktTabs);
Vue.component('lkt-table', LktTable);

/**
 * Automatically include all vue-lkt fields
 */
import LktFieldApiFile from "./vue/components/fields/LktFieldApiFile";
import LktFieldApiSelect from "./vue/components/fields/LktFieldApiSelect";
import LktFieldCheck from "./vue/components/fields/LktFieldCheck";
import LktFieldDate from "./vue/components/fields/LktFieldDate";
import LktFieldEditor from "./vue/components/fields/LktFieldEditor";
import LktFieldEmail from "./vue/components/fields/LktFieldEmail";
import LktFieldFileSelect from "./vue/components/fields/LktFieldFileSelect";
import LktFieldImage from "./vue/components/fields/LktFieldImage";
import LktFieldPassword from "./vue/components/fields/LktFieldPassword";
import LktFieldRadio from "./vue/components/fields/LktFieldRadio";
import LktFieldSearchSelect from "./vue/components/fields/LktFieldSearchSelect";
import LktFieldSwitch from "./vue/components/fields/LktFieldSwitch";
import LktFieldTel from "./vue/components/fields/LktFieldTel";
import LktFieldText from "./vue/components/fields/LktFieldText";
import LktFieldTextArea from "./vue/components/fields/LktFieldTextArea";
import LktFieldUnit from "./vue/components/fields/LktFieldUnit";
import LktFieldSelect from "./vue/components/fields/LktFieldSelect";

Vue.component('lkt-field-api-file', LktFieldApiFile);
Vue.component('lkt-field-api-select', LktFieldApiSelect);
Vue.component('lkt-field-check', LktFieldCheck);
Vue.component('lkt-field-date', LktFieldDate);
Vue.component('lkt-field-editor', LktFieldEditor);
Vue.component('lkt-field-email', LktFieldEmail);
Vue.component('lkt-field-file-select', LktFieldFileSelect);
Vue.component('lkt-field-image', LktFieldImage);
Vue.component('lkt-field-password', LktFieldPassword);
Vue.component('lkt-field-radio', LktFieldRadio);
Vue.component('lkt-field-search-select', LktFieldSearchSelect);
Vue.component('lkt-field-switch', LktFieldSwitch);
Vue.component('lkt-field-tel', LktFieldTel);
Vue.component('lkt-field-text', LktFieldText);
Vue.component('lkt-field-text-area', LktFieldTextArea);
Vue.component('lkt-field-unit', LktFieldUnit);
Vue.component('lkt-field-select', LktFieldSelect);

/**
 * Create apì environments
 */
import {Environment} from "./js/http/Environment";


/**
 * Environments
 */
Environment.create({
    name: 'dev',
    url: ''
});

Environment.create({
    name: 'prod',
    url: '',
});


/**
 * Export data
 */
export {$, axios, Vue, VueRouter, Environment};