import {StringHelper} from "../../js/helpers/StringHelper";

var $ = require('jquery');

export const Select2Helper = {
    CACHE: {},

    create(selector, select2Options = {}) {
        let $items = $(selector);
        $items.each((i, item) => {
            let $item = $(item),
                name = Select2Helper.getStoreName($item);

            if (!Select2Helper.CACHE[name]) {
                Select2Helper.CACHE[name] = $item.select2(select2Options);
            }
        });
    },

    get(selector) {
        let $items = $(selector),
            r = [];

        $items.each((i, item) => {
            let $item = $(item),
                name = Select2Helper.getStoreName($item);

            if (Select2Helper.CACHE[name]) {
                r.push(Select2Helper.CACHE[name]);
            }
        });

        return r;
    },

    rebuild(selector, select2Options = {}) {
        let $items = $(selector);
        $items.each((i, item) => {
            let $item = $(item),
                name = Select2Helper.getStoreName($item);

            if (!Select2Helper.CACHE[name]) {
                Select2Helper.CACHE[name] = $item.select2(select2Options);
            } else {
                Select2Helper.CACHE[name].select2('destroy').select2(select2Options);
            }
        });
    },

    value($item, value, select2Options = {}) {
        $item.val(value);
        let name = Select2Helper.getStoreName($item);
        Select2Helper.CACHE[name].select2('destroy').select2(select2Options);
    },

    getStoreName($item) {
        let name = StringHelper.kebabCaseToCamelCase($item.attr('name')),
            id = StringHelper.kebabCaseToCamelCase($item.attr('id'));

        if (id) {
            return id;
        }
        return name;
    }
};