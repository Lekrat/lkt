$.fn.lktFindSelectsBySelectedValue = function (value) {
    this.filteredByValue = value;
    let $items = $(this).find('select');
    let $r = undefined;
    $items.each((i, item) => {
        let $item = $(item);

        if ($item.val() === value){
            if (typeof $r === 'undefined'){
                $r = $item;
            } else {
                $r = $r.add($item);
            }
        }
    });

    this.$items = $r;
    return $r;
};