import {TypeHelper} from "../../js/helpers/TypeHelper";
import {ObjectHelper} from "../../js/helpers/ObjectHelper";

window.$.fn.lktHtmlValidation = function (options = {}) {

    this.options = ObjectHelper.merge(ObjectHelper.clone({
        triggers: ['change'],
        onValid: undefined,
        onInvalid: undefined,
    }), options);

    if (!TypeHelper.isArray(this.options.triggers)) {
        this.options.triggers = [];
    }

    let callback = (e, $item) => {
        let isValid = $item.get(0).checkValidity();

        if (isValid && TypeHelper.isFunction(this.options.onValid)) {
            this.options.onValid(e, $item);
        } else if (!isValid && TypeHelper.isFunction(this.options.onInvalid)) {
            this.options.onInvalid(e, $item);
        }
    };

    this.options.triggers.forEach(trigger => {
        $(this).each((i, item) => {
            let $item = $(item);
            $item.on(trigger, (e) => { callback(e, $item) });
            callback(undefined, $item);
        });
    });
};