import {Lkt} from "./modules/lkt";
import {LktHelpers} from "./modules/lkt-helpers";
import {LktHttp} from "./modules/lkt-http";

let _lkt = new Lkt();

LktHelpers(_lkt);
LktHttp(_lkt);

export const lkt = _lkt;