export class TypeHelper {

    static isArray(mixedVar: any) {
        if (typeof Array.isArray === 'undefined') {
            // @ts-ignore
            Array.isArray = function(obj) {
                return Object.prototype.toString.call(obj) === '[object Array]';
            }
        }

        return Array.isArray(mixedVar);
    }

    static isFunction(fn: any) {
        return typeof (fn) === 'function';
    }

    static isNumeric(value: any) {
        let v = String(value).replace(',', '.'),
            v1 = parseFloat(v);
        return !isNaN(v1)
            && isFinite(v1);
    }

    static isString(value: any) {
        return typeof value === 'string';
    }

    static isObject(mixedVar: any) {
        return typeof (mixedVar) === 'object';
    }

    static isUndefined(mixedVar: any) {
        return typeof (mixedVar) === 'undefined';
    }

    static is(v: any) {
        return typeof v !== "undefined" || v !== null;
    }

    static isset() {
        let a = arguments,
            l = a.length,
            i = 0,
            undef;

        if (l === 0) {
            throw new Error('Empty isset');
        }

        while (i !== l) {
            if (a[i] === undef || a[i] === null) {
                return false;
            }
            i++;
        }
        return true;
    }
}