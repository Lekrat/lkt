import {TypeHelper} from "./TypeHelper";

export class ObjectHelper {

    static clone(obj: object) {
        if (typeof obj === 'object') {
            return Object.assign( Object.create( Object.getPrototypeOf(obj)), obj);
        }
        return {};
    }

    static sort(obj: any) {

        // All browsers solution
        let r = [];

        for (let prop in obj) {

            let data = obj[prop];

            if (TypeHelper.isObject(data)) {
                data = ObjectHelper.sort(data);
            }

            r.push([prop, data]);
        }

        r.sort(function(a, b) {
            return a[0] - b[0];
        });

        return r.map(z => { return z[1];});


        // ES8 Solution
        // let r = Object.entries(obj)
        //     .sort(([,a],[,b]) => a-b)
        //     .reduce((r, [k, v]) => ({ ...r, [k]: v }), {});


        // ES10 Solution
        // let r = Object.fromEntries(
        //     Object.entries(data).sort(([,a],[,b]) => a-b)
        // );
    }

    static fetch(obj: any, key: any) {
        let args = key.split('.');
        let argsLength = args.length;
        let c = 0;
        let t = obj;

        // Parse config data and fetch attribute
        while (typeof (t[args[c]]) !== 'undefined') {
            t = t[args[c]];
            ++c;
        }

        // If not found...
        if (c < argsLength) {
            //t = '';
            t = null;
        }

        return t;
    }

    static merge (obj1: any, obj2: any) {
        for (let key in obj2) {
            if (obj2.hasOwnProperty(key)) {
                obj1[key] = obj2[key];
            }
        }
        return obj1;
    }

    static safeMerge (obj1: any, obj2: any) {
        for (let key in obj2) {
            if (obj1.hasOwnProperty(key) && obj2.hasOwnProperty(key)) {
                obj1[key] = obj2[key];
            }
        }
        return obj1;
    }

    static deleteKeys(obj: any, keys: any[]){

        keys.forEach(key => {
            if (obj.hasOwnProperty(key)){
                delete obj[key];
            }
        });

        return obj;
    }

    static toArray(t: any) {
        return Object.keys(t).map(function (e) {
            return t[e]
        })
    }
}