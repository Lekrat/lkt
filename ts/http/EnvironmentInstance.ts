import {TypeHelper} from "../helpers/TypeHelper";

class ResourceInstance implements EnvironmentInterface {
    name: string = undefined;
    url: string = undefined;
    method: string = undefined;
    environment: string = undefined;
    unsafeParams: boolean = false;
    params: object = {};
    renameParams: object = {};
    fillLeftSeparator: string = '{';
    fillRightSeparator: string = '}';
    success: string = undefined;
    validStatuses: number[] = [200, 201, 202];
    searchParam: string = undefined;
    processResults: any = undefined;
    escapeMarkup: any = undefined;
    templateResult: any = undefined;
    templateSelection: any = undefined;
    paginationVariable: string = 'paged';
    enableMultipleCalling: boolean = false;
    isFileUpload: boolean = false;
    lastPageChecker: Function = () => {
        return false;
    };
    dataType: string = 'json';
    cache: object = {};
    cacheTime: number = 0;

    forceRefreshFlag: boolean = false;

    isFetching: boolean = false;
    inLastPage: boolean = false;
    currentPage: number = 0;

    constructor(data: EnvironmentInterface) {

        if (data.name) {
            this.name = data.name;
        }

        if (data.url) {
            this.url = data.url;
        }

        if (data.method) {
            this.method = data.method;
        }

        if (data.environment) {
            this.environment = data.environment;
        }

        if (data.unsafeParams) {
            this.unsafeParams = data.unsafeParams;
        }

        if (data.params) {
            this.params = data.params;
        }

        if (data.renameParams) {
            this.renameParams = data.renameParams;
        }

        if (data.fillLeftSeparator) {
            this.fillLeftSeparator = data.fillLeftSeparator;
        }

        if (data.fillRightSeparator) {
            this.fillRightSeparator = data.fillRightSeparator;
        }

        if (data.success) {
            this.success = data.success;
        }

        if (data.validStatuses) {
            this.validStatuses = data.validStatuses;
        }

        if (data.searchParam) {
            this.searchParam = data.searchParam;
        }

        if (data.processResults) {
            this.processResults = data.processResults;
        }

        if (data.escapeMarkup) {
            this.escapeMarkup = data.escapeMarkup;
        }

        if (data.templateResult) {
            this.templateResult = data.templateResult;
        }

        if (data.templateSelection) {
            this.templateSelection = data.templateSelection;
        }

        if (data.paginationVariable) {
            this.paginationVariable = data.paginationVariable;
        }

        if (data.enableMultipleCalling) {
            this.enableMultipleCalling = data.enableMultipleCalling;
        }

        if (data.isFileUpload) {
            this.isFileUpload = data.isFileUpload;
        }

        if (data.lastPageChecker) {
            this.lastPageChecker = data.lastPageChecker;
        }

        if (data.dataType) {
            this.dataType = data.dataType;
        }

        if (data.cache) {
            this.cache = data.cache;
        }

        if (data.cacheTime) {
            this.cacheTime = data.cacheTime;
        }

        if (data.forceRefreshFlag) {
            this.forceRefreshFlag = data.forceRefreshFlag;
        }
    }


    forceRefresh() {
        this.forceRefreshFlag = true;
    }

    page(page:number = undefined, params:any = {}, environment:string = this.environment) {

        if (this.isFetching && !this.enableMultipleCalling){
            return new Promise( (resolve, reject) => {
                resolve(undefined);
            });
        }

        if (TypeHelper.isUndefined(page) && TypeHelper.isUndefined(this.currentPage)){
            page = 1;
        } else if (TypeHelper.isUndefined(page) && !TypeHelper.isUndefined(this.currentPage)){
            page = this.currentPage;
        }

        this.currentPage = page;

        if (this.inLastPage && this.currentPage !== 1){
            return new Promise( (resolve, reject) => {
                resolve(undefined);
            });
        }

        params[this.paginationVariable] = this.currentPage;
        let data = this.build(params, environment);

        switch (data.method) {
            case 'get':
            case 'post':
                this.isFetching = true;
                return axios(data).then(promise => {
                    ++this.currentPage;
                    this.isFetching = false;
                    this.inLastPage = this.lastPageChecker(promise);
                    if (TypeHelper.isFunction(this.success)) {
                        return this.success(promise);
                    }
                    return promise;
                }).catch(error => {
                    this.isFetching = false;
                    this.inLastPage = this.lastPageChecker(promise);
                    return error;
                });

            default:
                console.warn('Error: Invalid method');
        }
    };
}

// export function ResourceInstance2 (data = {}){
//
//     data = ObjectHelper.merge(ObjectHelper.clone(Resource.DEFAULT_DATA), data);
//
//     this.name = data.name;
//     this.url = data.url;
//     this.method = data.method;
//     this.params = data.params;
//     this.environment = data.environment;
//     this.unsafeParams = data.unsafeParams;
//     this.renameParams = data.renameParams;
//     this.fillLeftSeparator = data.fillLeftSeparator;
//     this.fillRightSeparator = data.fillRightSeparator;
//     this.success = data.success;
//     this.validStatuses = data.validStatuses;
//     this.searchParam = data.searchParam;
//     this.processResults = data.processResults;
//     this.dataType = data.dataType;
//     this.paginationVariable = data.paginationVariable;
//     this.enableMultipleCalling = data.enableMultipleCalling;
//     this.lastPageChecker = data.lastPageChecker;
//     this.currentPage = undefined;
//     this.isFetching = false;
//     this.inLastPage = false;
//     this.escapeMarkup = data.escapeMarkup;
//     this.templateResult = data.templateResult;
//     this.templateSelection = data.templateSelection;
//     this.isFileUpload = data.isFileUpload;
//     this.cache = {};
//     this.cacheTime = data.cacheTime;
//     this.forceRefreshFlag = false;
//
//     this.forceRefresh = function() {
//         this.forceRefreshFlag = true;
//     }
//
//     this.page = function(page = undefined, params = {}, environment = this.environment){
//
//         if (this.isFetching && !this.enableMultipleCalling){
//             return new Promise( (resolve, reject) => {
//                 resolve(undefined);
//             });
//         }
//
//         if (TypeHelper.isUndefined(page) && TypeHelper.isUndefined(this.currentPage)){
//             page = 1;
//         } else if (TypeHelper.isUndefined(page) && !TypeHelper.isUndefined(this.currentPage)){
//             page = this.currentPage;
//         }
//
//         this.currentPage = page;
//
//         if (this.inLastPage && this.currentPage !== 1){
//             return new Promise( (resolve, reject) => {
//                 resolve(undefined);
//             });
//         }
//
//         params[this.paginationVariable] = this.currentPage;
//         let data = this.build(params, environment);
//
//         switch (data.method) {
//             case 'get':
//             case 'post':
//                 this.isFetching = true;
//                 return axios(data).then(promise => {
//                     ++this.currentPage;
//                     this.isFetching = false;
//                     this.inLastPage = this.lastPageChecker(promise);
//                     if (TypeHelper.isFunction(this.success)) {
//                         return this.success(promise);
//                     }
//                     return promise;
//                 }).catch(error => {
//                     this.isFetching = false;
//                     this.inLastPage = this.lastPageChecker(promise);
//                     return error;
//                 });
//
//             default:
//                 console.warn('Error: Invalid method');
//         }
//     };
//
//     this.call = function(params = {}, environment = this.environment) {
//
//         if (this.isFetching && !this.enableMultipleCalling){
//             return new Promise( (resolve, reject) => {
//                 resolve(undefined);
//             });
//         }
//
//         let data = this.build(params, environment);
//
//         if (data.method === 'get' && this.cacheTime > 0 && !this.forceRefreshFlag && this.cache[data.url]){
//             let now = TimeHelper.time();
//             let limit = this.cache[data.url].moment + this.cacheTime;
//
//             if (limit - now > 0){
//                 return new Promise((resolve, reject) => {
//                     return resolve((() => {
//                         if (TypeHelper.isFunction(this.success)) {
//                             return this.success(this.cache[data.url].r);
//                         }
//                         return this.cache[data.url].r;
//                     })());
//                 });
//             }
//         }
//
//         switch (data.method) {
//             case 'get':
//             case 'post':
//             case 'put':
//             case 'delete':
//                 this.isFetching = true;
//                 return axios(data).then(promise => {
//                     this.isFetching = false;
//                     if (data.method === 'get' && this.cacheTime > 0){
//                         this.cache[data.url] = {
//                             moment: TimeHelper.time(),
//                             r: promise,
//                         };
//                         this.forceRefreshFlag = false;
//                     }
//
//                     if (TypeHelper.isFunction(this.success)) {
//                         return this.success(promise);
//                     }
//                     return promise;
//                 }).catch(error => {
//                     this.isFetching = false;
//                     return Promise.reject(new Error(error));
//                 });
//             case 'download':
//             case 'open':
//                 return axios.get(data.url, {'responseType': 'blob'}).then(r => {
//                     let contentDisposition = r.headers['content-disposition'],
//                         fileName = '';
//                     if (contentDisposition){
//                         contentDisposition = contentDisposition.split(';');
//                         contentDisposition.forEach(z => {
//                             let y = z.split('=');
//                             if (StringHelper.trim(y[0]) === 'filename'){
//                                 let n = StringHelper.trim(y[1]);
//                                 n = StringHelper.trim(n, '"');
//                                 fileName = n;
//                             }
//                         });
//                     }
//                     window.download(r.data, fileName);
//                     if (TypeHelper.isFunction(this.success)) {
//                         return this.success(promise);
//                     }
//                     return r;
//                 }).catch(error => {
//                     return error;
//                 });
//
//             default:
//                 console.warn('Error: Invalid method');
//         }
//     };
//
//     this.build = function(args = {}, environment = undefined) {
//         let p = ObjectHelper.clone(this.params);
//         let r = {};
//         for (let k in p) {
//             if (p.hasOwnProperty(k)) {
//                 r[k] = p[k].default;
//             }
//         }
//
//         for (let key in args) {
//             if (this.unsafeParams || key === this.paginationVariable || (args.hasOwnProperty(key) && this.params.hasOwnProperty(key))) {
//                 if (this.renameParams.hasOwnProperty(key)) {
//                     delete r[key];
//                     r[this.renameParams[key]] = args[key];
//                 } else {
//                     r[key] = args[key];
//                 }
//
//                 if (TypeHelper.isUndefined(r[key])) {
//                     delete r[key];
//                 }
//             }
//         }
//
//         let url = this.url;
//         let env = Environment.get(environment);
//         let auth = {};
//
//         if (env && env.url) {
//             url = env.url + url;
//             if (!TypeHelper.isUndefined(env.auth) && !TypeHelper.isUndefined(env.auth.user)){
//                 auth = env.auth;
//             }
//         }
//
//         let toDelete = StringHelper.extractFillData(url, r, this.fillLeftSeparator, this.fillRightSeparator);
//         let link = StringHelper.fill(url, r, this.fillLeftSeparator, this.fillRightSeparator);
//         r = ObjectHelper.deleteKeys(r, toDelete);
//
//         // let _url = new URL(document.location.href.split('?')[0]);
//         //
//         // if (link.indexOf('/') === 0){
//         //     link = _url.origin + link;
//         // }
//
//         if (this.method === 'get' || this.method === 'open') {
//             let stringParams = Resource.paramsToString(r);
//             link = [link, stringParams].join('?');
//             r = {};
//         }
//
//         let headers = undefined;
//         if (this.isFileUpload) {
//             headers = {
//                 'Content-Type': 'multipart/form-data'
//             };
//
//             let formData = new FormData();
//             for (let k in r){
//                 if (r.hasOwnProperty(k)){
//                     formData.append(k, r[k]);
//                 }
//             }
//             r = formData;
//         }
//
//         return {
//             url: link,
//             method: this.method.toLowerCase(),
//             data: r,
//             auth: auth,
//             validateStatus: (status) => this.validateStatus(status),
//             headers: headers
//         }
//     };
//
//     this.validateStatus = function(status) {
//         if (this.validStatuses.length === 0) {
//             return true;
//         }
//         return this.validStatuses.indexOf(status) !== -1;
//     };
//
// }