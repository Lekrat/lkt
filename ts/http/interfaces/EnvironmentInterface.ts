interface EnvironmentInterface {
    name: string;
    url: string;
    method: string;
    environment: string;
    unsafeParams: boolean;
    params: object;
    renameParams: object;
    fillLeftSeparator: string;
    fillRightSeparator: string;
    success: string;
    validStatuses: number[];
    searchParam: string;
    processResults: any;
    escapeMarkup: any;
    templateResult: any;
    templateSelection: any;
    paginationVariable: string;
    enableMultipleCalling: boolean;
    isFileUpload: boolean;
    lastPageChecker: Function;
    dataType: string;
    cache: object;
    cacheTime: number;
    forceRefreshFlag: boolean;
}