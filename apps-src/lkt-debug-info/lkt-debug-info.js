require('../../bootstrap');

import LktDebugInfo from "../../vue/components/debug/LktDebugInfo";
Vue.component('lkt-debug-info', LktDebugInfo);

if (!window.Lkt) {
    window.Lkt = {};
}
import {LktMixin} from "../../vue/mixins/LktMixin";

Vue.mixin(LktMixin);

window.Lkt.DebugInfo = new Vue({
    el: '#lkt-debug-info',
    mounted(){
        var ComponentClass = Vue.extend(LktDebugInfo);
        var instance = new ComponentClass();
        instance.$mount();
        this.$el.appendChild(instance.$el)
    }
});