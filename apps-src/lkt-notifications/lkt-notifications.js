require('../../bootstrap');

import LktNotifications from "../../vue/components/LktNotifications";
Vue.component('lkt-notifications', LktNotifications);

if (!window.Lkt) {
    window.Lkt = {};
}
import {LktMixin} from "../../vue/mixins/LktMixin";

Vue.mixin(LktMixin);

window.Lkt.Notifications = new Vue({
    el: '#lkt-notifications',
    methods: {
        push(notification) {
            this.$refs.notifications.push(notification);
        }
    },
    mounted(){
        var ComponentClass = Vue.extend(LktNotifications);
        var instance = new ComponentClass();
        instance.$root = this;
        instance.$mount();
        this.$children.push(instance);
        this.$el.append(instance.$el);
        this.$refs.notifications = instance;
    }
});