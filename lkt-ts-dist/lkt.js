/******/ (() => { // webpackBootstrap
/******/ 	"use strict";
/******/ 	var __webpack_modules__ = ({

/***/ "./js/classes/form/Validation.js":
/*!***************************************!*\
  !*** ./js/classes/form/Validation.js ***!
  \***************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Validation": () => (/* binding */ Validation)
/* harmony export */ });
/* harmony import */ var _helpers_TypeHelper__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../helpers/TypeHelper */ "./js/helpers/TypeHelper.js");
function _classCallCheck(instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
}

function _defineProperties(target, props) {
  for (var i = 0; i < props.length; i++) {
    var descriptor = props[i];
    descriptor.enumerable = descriptor.enumerable || false;
    descriptor.configurable = true;
    if ("value" in descriptor) descriptor.writable = true;
    Object.defineProperty(target, descriptor.key, descriptor);
  }
}

function _createClass(Constructor, protoProps, staticProps) {
  if (protoProps) _defineProperties(Constructor.prototype, protoProps);
  if (staticProps) _defineProperties(Constructor, staticProps);
  Object.defineProperty(Constructor, "prototype", {
    writable: false
  });
  return Constructor;
}


var Validation = /*#__PURE__*/function () {
  function Validation(name, fn) {
    _classCallCheck(this, Validation);

    this.name = name;
    this.evaluator = _helpers_TypeHelper__WEBPACK_IMPORTED_MODULE_0__.TypeHelper.isFunction(fn) ? fn : function () {
      return true;
    };
  }

  _createClass(Validation, [{
    key: "validate",
    value: function validate(value) {
      return this.evaluator(value);
    }
  }]);

  return Validation;
}();

/***/ }),

/***/ "./js/dom/ScrollHelper.js":
/*!********************************!*\
  !*** ./js/dom/ScrollHelper.js ***!
  \********************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ScrollHelper": () => (/* binding */ ScrollHelper)
/* harmony export */ });
var ScrollHelper = {
  getDocumentHeight: function getDocumentHeight() {
    var D = document;
    return Math.max(D.body.scrollHeight, D.documentElement.scrollHeight, D.body.offsetHeight, D.documentElement.offsetHeight, D.body.clientHeight, D.documentElement.clientHeight);
  },
  getWindowScrollPosition: function getWindowScrollPosition() {
    var supportPageOffset = window.pageXOffset !== undefined,
        isCSS1Compat = (document.compatMode || "") === "CSS1Compat",
        x = supportPageOffset ? window.pageXOffset : isCSS1Compat ? document.documentElement.scrollLeft : document.body.scrollLeft,
        y = supportPageOffset ? window.pageYOffset : isCSS1Compat ? document.documentElement.scrollTop : document.body.scrollTop;
    return {
      x: x,
      y: y
    };
  },
  getWindowSize: function getWindowSize() {
    var w = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth,
        h = window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight;
    return {
      w: w,
      h: h
    };
  },
  getElementSize: function getElementSize(element) {
    return {
      h: element.offsetHeight,
      w: element.offsetWidth
    };
  },
  windowScrollAtBottom: function windowScrollAtBottom() {
    return ScrollHelper.getWindowScrollPosition().y + ScrollHelper.getWindowSize().h >= ScrollHelper.getDocumentHeight();
  },
  elementScrollAtBottom: function elementScrollAtBottom(element) {
    return element.scrollTop + ScrollHelper.getElementSize(element).h >= element.scrollHeight;
  }
};

/***/ }),

/***/ "./js/helpers/ArrayHelper.js":
/*!***********************************!*\
  !*** ./js/helpers/ArrayHelper.js ***!
  \***********************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ArrayHelper": () => (/* binding */ ArrayHelper)
/* harmony export */ });
var ArrayHelper = {
  count: function count(t, e) {
    var r,
        n = 0;

    if (null === t || "undefined" == typeof t) {
      return 0;
    }

    if (t.constructor !== Array && t.constructor !== Object) {
      return 1;
    }

    "COUNT_RECURSIVE" === e && (e = 1), 1 != e && (e = 0);

    for (r in t) {
      t.hasOwnProperty(r) && (n++, 1 != e || !t[r] || t[r].constructor !== Array && t[r].constructor !== Object || (n += this.count(t[r], 1)));
    }

    return n;
  },
  inArray: function inArray(needle, haystack, argStrict) {
    var key = '';
    var strict = !!argStrict;

    if (strict) {
      for (key in haystack) {
        if (haystack[key] === needle) {
          return key;
        }
      }
    } else {
      for (key in haystack) {
        if (haystack[key] == needle) {
          return true;
        }
      }
    }

    return false;
  },
  range: function range(start, end) {
    var array = [];

    for (var i = start; i < end; i++) {
      array.push(i);
    }

    return array;
  },
  removeDuplicates: function removeDuplicates(myArr, prop) {
    return myArr.filter(function (obj, pos, arr) {
      return arr.map(function (mapObj) {
        return mapObj[prop];
      }).indexOf(obj[prop]) === pos;
    });
  },
  getUniques: function getUniques(arr, comp) {
    return arr.map(function (e) {
      return e[comp];
    }) // store the keys of the unique objects
    .map(function (e, i, _final) {
      return _final.indexOf(e) === i && i;
    }) // eliminate the dead keys & store unique objects
    .filter(function (e) {
      return arr[e];
    }).map(function (e) {
      return arr[e];
    });
  },
  clone: function clone(arr) {
    return JSON.parse(JSON.stringify(arr));
  }
};

/***/ }),

/***/ "./js/helpers/DocumentHelper.js":
/*!**************************************!*\
  !*** ./js/helpers/DocumentHelper.js ***!
  \**************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "DocumentHelper": () => (/* binding */ DocumentHelper)
/* harmony export */ });
function _classCallCheck(instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
}

function _defineProperties(target, props) {
  for (var i = 0; i < props.length; i++) {
    var descriptor = props[i];
    descriptor.enumerable = descriptor.enumerable || false;
    descriptor.configurable = true;
    if ("value" in descriptor) descriptor.writable = true;
    Object.defineProperty(target, descriptor.key, descriptor);
  }
}

function _createClass(Constructor, protoProps, staticProps) {
  if (protoProps) _defineProperties(Constructor.prototype, protoProps);
  if (staticProps) _defineProperties(Constructor, staticProps);
  Object.defineProperty(Constructor, "prototype", {
    writable: false
  });
  return Constructor;
}

var DocumentHelper = /*#__PURE__*/function () {
  function DocumentHelper() {
    _classCallCheck(this, DocumentHelper);
  }

  _createClass(DocumentHelper, null, [{
    key: "cleanEmptyNodes",
    value: function cleanEmptyNodes(node) {
      for (var n = 0; n < node.childNodes.length; n++) {
        var child = node.childNodes[n];

        if (child.nodeType === 8 || child.nodeType === 3 && !/\S/.test(child.nodeValue)) {
          node.removeChild(child);
          n--;
        } else if (child.nodeType === 1) {
          Lkt.Document.Helper.cleanEmptyNodes(child);
        }
      }

      return true;
    }
  }]);

  return DocumentHelper;
}();

/***/ }),

/***/ "./js/helpers/FileHelper.js":
/*!**********************************!*\
  !*** ./js/helpers/FileHelper.js ***!
  \**********************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "FileHelper": () => (/* binding */ FileHelper)
/* harmony export */ });
var FileHelper = {
  baseName: function baseName(str) {
    str = String(str);
    var split = '/';

    if (str.indexOf('C:\\') === 0) {
      split = '\\';
    }

    var base = str.substring(str.lastIndexOf(split) + 1);

    if (base.lastIndexOf('.') !== -1) {
      base = base.substring(0, base.lastIndexOf('.'));
    }

    return base;
  }
};

/***/ }),

/***/ "./js/helpers/FunctionHelper.js":
/*!**************************************!*\
  !*** ./js/helpers/FunctionHelper.js ***!
  \**************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "FunctionHelper": () => (/* binding */ FunctionHelper)
/* harmony export */ });
/* harmony import */ var _TypeHelper__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./TypeHelper */ "./js/helpers/TypeHelper.js");

var FunctionHelper = {
  clone: function clone(that) {
    if (_TypeHelper__WEBPACK_IMPORTED_MODULE_0__.TypeHelper.isFunction(that)) {
      var temp = function temporary() {
        return that.apply(this, arguments);
      };

      for (var key in this) {
        if (this.hasOwnProperty(key)) {
          temp[key] = this[key];
        }
      }

      return temp;
    }

    return undefined;
  }
};

/***/ }),

/***/ "./js/helpers/ImageHelper.js":
/*!***********************************!*\
  !*** ./js/helpers/ImageHelper.js ***!
  \***********************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ImageHelper": () => (/* binding */ ImageHelper)
/* harmony export */ });
/* harmony import */ var _TypeHelper__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./TypeHelper */ "./js/helpers/TypeHelper.js");
function _classCallCheck(instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
}

function _defineProperties(target, props) {
  for (var i = 0; i < props.length; i++) {
    var descriptor = props[i];
    descriptor.enumerable = descriptor.enumerable || false;
    descriptor.configurable = true;
    if ("value" in descriptor) descriptor.writable = true;
    Object.defineProperty(target, descriptor.key, descriptor);
  }
}

function _createClass(Constructor, protoProps, staticProps) {
  if (protoProps) _defineProperties(Constructor.prototype, protoProps);
  if (staticProps) _defineProperties(Constructor, staticProps);
  Object.defineProperty(Constructor, "prototype", {
    writable: false
  });
  return Constructor;
}


var ImageHelper = /*#__PURE__*/function () {
  function ImageHelper() {
    _classCallCheck(this, ImageHelper);
  }

  _createClass(ImageHelper, null, [{
    key: "isBase64",
    value: function isBase64() {
      var src = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '';
      return _TypeHelper__WEBPACK_IMPORTED_MODULE_0__.TypeHelper.isString(src) && src !== '' && src.indexOf('data:image/') !== -1;
    }
  }]);

  return ImageHelper;
}();

/***/ }),

/***/ "./js/helpers/NavigatorHelper.js":
/*!***************************************!*\
  !*** ./js/helpers/NavigatorHelper.js ***!
  \***************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "NavigatorHelper": () => (/* binding */ NavigatorHelper)
/* harmony export */ });
function _typeof(obj) {
  "@babel/helpers - typeof";

  return _typeof = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (obj) {
    return typeof obj;
  } : function (obj) {
    return obj && "function" == typeof Symbol && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj;
  }, _typeof(obj);
}

function _classCallCheck(instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
}

function _defineProperties(target, props) {
  for (var i = 0; i < props.length; i++) {
    var descriptor = props[i];
    descriptor.enumerable = descriptor.enumerable || false;
    descriptor.configurable = true;
    if ("value" in descriptor) descriptor.writable = true;
    Object.defineProperty(target, descriptor.key, descriptor);
  }
}

function _createClass(Constructor, protoProps, staticProps) {
  if (protoProps) _defineProperties(Constructor.prototype, protoProps);
  if (staticProps) _defineProperties(Constructor, staticProps);
  Object.defineProperty(Constructor, "prototype", {
    writable: false
  });
  return Constructor;
}

var NavigatorHelper = /*#__PURE__*/function () {
  function NavigatorHelper() {
    _classCallCheck(this, NavigatorHelper);
  }

  _createClass(NavigatorHelper, null, [{
    key: "supportsPromises",
    value: function supportsPromises() {
      return typeof Promise !== "undefined" && Promise.toString().indexOf("[native code]") !== -1;
    }
  }, {
    key: "supportsLocalStorage",
    value: function supportsLocalStorage() {
      var supportsLocalStorage = 1;

      if ((typeof localStorage === "undefined" ? "undefined" : _typeof(localStorage)) === 'object') {
        try {
          localStorage.setItem('localStorage', 1);
          localStorage.removeItem('localStorage');
        } catch (e) {
          supportsLocalStorage = -1;
        }
      } else {
        supportsLocalStorage = -1;
      }

      return supportsLocalStorage === 1;
    }
  }, {
    key: "supportsRgba",
    value: function supportsRgba() {
      var scriptElement = document.getElementsByTagName('script')[0],
          prevColor = scriptElement.style.color,
          r = true;

      try {
        scriptElement.style.color = 'rgba(1,5,13,0.44)';
        scriptElement.style.color = prevColor;
      } catch (e) {
        r = false;
      }

      return r;
    }
  }, {
    key: "isAndroid",
    value: function isAndroid() {
      return /(android)/i.test(navigator.userAgent);
    }
  }, {
    key: "isAndroid5",
    value: function isAndroid5() {
      return /(android 5)/i.test(navigator.userAgent);
    }
  }, {
    key: "isSafari",
    value: function isSafari() {
      return /(safari)/i.test(navigator.userAgent) && !NavigatorHelper.isChrome();
    }
  }, {
    key: "isChrome",
    value: function isChrome() {
      return /(chrome)/i.test(navigator.userAgent);
    }
  }, {
    key: "isIPhone",
    value: function isIPhone() {
      return /(iPhone|iPod)/i.test(navigator.userAgent);
    }
  }, {
    key: "isIPad",
    value: function isIPad() {
      return /(iPad)/i.test(navigator.userAgent);
    }
  }, {
    key: "isMobile",
    value: function isMobile() {
      return /(mobile|iPhone|iPod)/i.test(navigator.userAgent);
    }
  }]);

  return NavigatorHelper;
}();

/***/ }),

/***/ "./js/helpers/NumberHelper.js":
/*!************************************!*\
  !*** ./js/helpers/NumberHelper.js ***!
  \************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "NumberHelper": () => (/* binding */ NumberHelper)
/* harmony export */ });
var NumberHelper = {
  format: function format(value, decimals, decimalPoint, thousandsSeparator) {
    var v = String(parseFloat(value).toFixed(decimals).replace(/\d(?=(\d{3})+\.)/g, '$&D'));
    v = String(v.replace('.', decimalPoint));
    v = v.replace(/D/g, thousandsSeparator);
    return v;
  },
  clean: function clean(value) {
    return value.replace(/\,/g, '').replace(/\./g, '').replace(/\D/g, '');
  }
};

/***/ }),

/***/ "./js/helpers/ObjectHelper.js":
/*!************************************!*\
  !*** ./js/helpers/ObjectHelper.js ***!
  \************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ObjectHelper": () => (/* binding */ ObjectHelper)
/* harmony export */ });
/* harmony import */ var _TypeHelper__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./TypeHelper */ "./js/helpers/TypeHelper.js");
function _typeof(obj) {
  "@babel/helpers - typeof";

  return _typeof = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (obj) {
    return typeof obj;
  } : function (obj) {
    return obj && "function" == typeof Symbol && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj;
  }, _typeof(obj);
}

function _classCallCheck(instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
}

function _defineProperties(target, props) {
  for (var i = 0; i < props.length; i++) {
    var descriptor = props[i];
    descriptor.enumerable = descriptor.enumerable || false;
    descriptor.configurable = true;
    if ("value" in descriptor) descriptor.writable = true;
    Object.defineProperty(target, descriptor.key, descriptor);
  }
}

function _createClass(Constructor, protoProps, staticProps) {
  if (protoProps) _defineProperties(Constructor.prototype, protoProps);
  if (staticProps) _defineProperties(Constructor, staticProps);
  Object.defineProperty(Constructor, "prototype", {
    writable: false
  });
  return Constructor;
}


var ObjectHelper = /*#__PURE__*/function () {
  function ObjectHelper() {
    _classCallCheck(this, ObjectHelper);
  }

  _createClass(ObjectHelper, null, [{
    key: "clone",
    value: function clone(obj) {
      if (_typeof(obj) === 'object') {
        return Object.assign(Object.create(Object.getPrototypeOf(obj)), obj);
      }

      return {};
    }
  }, {
    key: "sort",
    value: function sort(obj) {
      // All browsers solution
      var r = [];

      for (var prop in obj) {
        var data = obj[prop];

        if (_TypeHelper__WEBPACK_IMPORTED_MODULE_0__.TypeHelper.isObject(data)) {
          data = ObjectHelper.sort(data);
        }

        r.push([prop, data]);
      }

      r.sort(function (a, b) {
        return a[0] - b[0];
      });
      return r.map(function (z) {
        return z[1];
      }); // ES8 Solution
      // let r = Object.entries(obj)
      //     .sort(([,a],[,b]) => a-b)
      //     .reduce((r, [k, v]) => ({ ...r, [k]: v }), {});
      // ES10 Solution
      // let r = Object.fromEntries(
      //     Object.entries(data).sort(([,a],[,b]) => a-b)
      // );
    }
  }, {
    key: "fetch",
    value: function fetch(obj, key) {
      var args = key.split('.');
      var argsLength = args.length;
      var c = 0;
      var t = obj; // Parse config data and fetch attribute

      while (typeof t[args[c]] !== 'undefined') {
        t = t[args[c]];
        ++c;
      } // If not found...


      if (c < argsLength) {
        //t = '';
        t = null;
      }

      return t;
    }
  }, {
    key: "merge",
    value: function merge(obj1, obj2) {
      for (var key in obj2) {
        if (obj2.hasOwnProperty(key)) {
          obj1[key] = obj2[key];
        }
      }

      return obj1;
    }
  }, {
    key: "safeMerge",
    value: function safeMerge(obj1, obj2) {
      for (var key in obj2) {
        if (obj1.hasOwnProperty(key) && obj2.hasOwnProperty(key)) {
          obj1[key] = obj2[key];
        }
      }

      return obj1;
    }
  }, {
    key: "deleteKeys",
    value: function deleteKeys(obj, keys) {
      keys.forEach(function (key) {
        if (obj.hasOwnProperty(key)) {
          delete obj[key];
        }
      });
      return obj;
    }
  }, {
    key: "toArray",
    value: function toArray(t) {
      return Object.keys(t).map(function (e) {
        return t[e];
      });
    }
  }, {
    key: "jsonDecode",
    value: function jsonDecode(str_json) {
      var json = window.JSON;

      if ("object" == _typeof(json) && "function" == typeof json.parse) {
        try {
          return json.parse(str_json);
        } catch (err) {
          if (!(err instanceof SyntaxError)) {
            throw new Error("Unexpected error type in ObjectHelper.jsonDecode()");
          }

          return null;
        }
      }

      var cx = /[\u0000\u00ad\u0600-\u0604\u070f\u17b4\u17b5\u200c-\u200f\u2028-\u202f\u2060-\u206f\ufeff\ufff0-\uffff]/g,
          j,
          text = str_json;
      return cx.lastIndex = 0, cx.test(text) && (text = text.replace(cx, function (t) {
        return "\\u" + ("0000" + t.charCodeAt(0).toString(16)).slice(-4);
      })), /^[\],:{}\s]*$/.test(text.replace(/\\(?:["\\\/bfnrt]|u[0-9a-fA-F]{4})/g, "@").replace(/"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g, "]").replace(/(?:^|:|,)(?:\s*\[)+/g, "")) ? j = eval("(" + text + ")") : (this.php_js = this.php_js || {}, this.php_js.last_error_json = 4, null);
    }
  }]);

  return ObjectHelper;
}();

/***/ }),

/***/ "./js/helpers/SocialShareHelper.js":
/*!*****************************************!*\
  !*** ./js/helpers/SocialShareHelper.js ***!
  \*****************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "SocialShareHelper": () => (/* binding */ SocialShareHelper)
/* harmony export */ });
/* harmony import */ var _TypeHelper__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./TypeHelper */ "./js/helpers/TypeHelper.js");
/* harmony import */ var _StringHelper__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./StringHelper */ "./js/helpers/StringHelper.js");


var SocialShareHelper = {
  getMailUrl: function getMailUrl() {
    var email = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : undefined;
    var subject = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : undefined;
    var body = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : undefined;
    var r = new URL('mailto:' + email);

    if (!_TypeHelper__WEBPACK_IMPORTED_MODULE_0__.TypeHelper.isUndefined(subject)) {
      r.searchParams.append('subject', subject);
    }

    if (!_TypeHelper__WEBPACK_IMPORTED_MODULE_0__.TypeHelper.isUndefined(body)) {
      r.searchParams.append('body', body);
    }

    return r.href;
  },
  getFacebookUrl: function getFacebookUrl() {
    var url = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : undefined;

    if (_TypeHelper__WEBPACK_IMPORTED_MODULE_0__.TypeHelper.isUndefined(url)) {
      url = window.location.href;
    }

    var r = new URL('https://www.facebook.com/sharer/sharer.php');
    r.searchParams.append('u', url);
    return r.href;
  },
  getTwitterUrl: function getTwitterUrl() {
    var url = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : undefined;
    var via = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : undefined;
    var hashtags = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : [];

    if (_TypeHelper__WEBPACK_IMPORTED_MODULE_0__.TypeHelper.isUndefined(url)) {
      url = window.location.href;
    }

    var r = new URL('https://twitter.com/intent/tweet');
    r.searchParams.append('url', url);

    if (!_TypeHelper__WEBPACK_IMPORTED_MODULE_0__.TypeHelper.isUndefined(via)) {
      r.searchParams.append('via', via);
    }

    if (hashtags.length > 0) {
      r.searchParams.append('hashtags', hashtags.join(' '));
    }

    return r.href;
  },
  getGooglePlusUrl: function getGooglePlusUrl() {
    var url = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : undefined;

    if (_TypeHelper__WEBPACK_IMPORTED_MODULE_0__.TypeHelper.isUndefined(url)) {
      url = window.location.href;
    }

    var r = new URL('https://plus.google.com/share');
    r.searchParams.append('url', url);
    return r.href;
  },
  getLinkedInUrl: function getLinkedInUrl() {
    var url = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : undefined;
    var title = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : undefined;
    var summary = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : undefined;
    var source = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : undefined;

    if (_TypeHelper__WEBPACK_IMPORTED_MODULE_0__.TypeHelper.isUndefined(url)) {
      url = window.location.href;
    }

    var r = new URL('https://www.linkedin.com/shareArticle');
    r.searchParams.append('mini', 'true');
    r.searchParams.append('url', _StringHelper__WEBPACK_IMPORTED_MODULE_1__.StringHelper.htmlEntities(url));

    if (!_TypeHelper__WEBPACK_IMPORTED_MODULE_0__.TypeHelper.isUndefined(title)) {
      r.searchParams.append('title', _StringHelper__WEBPACK_IMPORTED_MODULE_1__.StringHelper.htmlEntities(title));
    }

    if (!_TypeHelper__WEBPACK_IMPORTED_MODULE_0__.TypeHelper.isUndefined(summary)) {
      r.searchParams.append('summary', _StringHelper__WEBPACK_IMPORTED_MODULE_1__.StringHelper.htmlEntities(summary));
    }

    if (!_TypeHelper__WEBPACK_IMPORTED_MODULE_0__.TypeHelper.isUndefined(source)) {
      r.searchParams.append('source', _StringHelper__WEBPACK_IMPORTED_MODULE_1__.StringHelper.htmlEntities(source));
    }

    return r.href;
  },
  getPinterestUrl: function getPinterestUrl() {
    var url = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : undefined;
    var media = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : undefined;
    var description = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : undefined;

    if (_TypeHelper__WEBPACK_IMPORTED_MODULE_0__.TypeHelper.isUndefined(url)) {
      url = window.location.href;
    }

    var r = new URL('//es.pinterest.com/pin/create/button/');
    r.searchParams.append('url', url);

    if (!_TypeHelper__WEBPACK_IMPORTED_MODULE_0__.TypeHelper.isUndefined(media)) {
      r.searchParams.append('media', media);
    }

    if (!_TypeHelper__WEBPACK_IMPORTED_MODULE_0__.TypeHelper.isUndefined(description)) {
      r.searchParams.append('description', description);
    }

    return r.href;
  },
  getWhatsAppUrl: function getWhatsAppUrl() {
    var url = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : undefined;
    var text = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : '';

    if (_TypeHelper__WEBPACK_IMPORTED_MODULE_0__.TypeHelper.isUndefined(url)) {
      url = window.location.href;
    }

    text = _StringHelper__WEBPACK_IMPORTED_MODULE_1__.StringHelper.htmlEntities(text) + ' ' + url;
    var r = new URL('whatsapp://send');
    r.searchParams.append('text', text);
    return r.href;
  }
};

/***/ }),

/***/ "./js/helpers/StringHelper.js":
/*!************************************!*\
  !*** ./js/helpers/StringHelper.js ***!
  \************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "StringHelper": () => (/* binding */ StringHelper)
/* harmony export */ });
function _typeof(obj) {
  "@babel/helpers - typeof";

  return _typeof = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (obj) {
    return typeof obj;
  } : function (obj) {
    return obj && "function" == typeof Symbol && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj;
  }, _typeof(obj);
}

function _classCallCheck(instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
}

function _defineProperties(target, props) {
  for (var i = 0; i < props.length; i++) {
    var descriptor = props[i];
    descriptor.enumerable = descriptor.enumerable || false;
    descriptor.configurable = true;
    if ("value" in descriptor) descriptor.writable = true;
    Object.defineProperty(target, descriptor.key, descriptor);
  }
}

function _createClass(Constructor, protoProps, staticProps) {
  if (protoProps) _defineProperties(Constructor.prototype, protoProps);
  if (staticProps) _defineProperties(Constructor, staticProps);
  Object.defineProperty(Constructor, "prototype", {
    writable: false
  });
  return Constructor;
}

var StringHelper = /*#__PURE__*/function () {
  function StringHelper() {
    _classCallCheck(this, StringHelper);
  }

  _createClass(StringHelper, null, [{
    key: "kebabCaseToCamelCase",
    value: function kebabCaseToCamelCase() {
      var str = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '';
      return str.replace(/-([a-z])/g, function (g) {
        return g[1].toUpperCase();
      });
    }
  }, {
    key: "explode",
    value: function explode(t, e, r) {
      if (arguments.length < 2 || "undefined" == typeof t || "undefined" == typeof e) {
        return null;
      }

      if ("" === t || t === !1 || null === t) {
        return !1;
      }

      if ("function" == typeof t || "object" == _typeof(t) || "function" == typeof e || "object" == _typeof(e)) {
        return {
          0: ""
        };
      }

      t === !0 && (t = "1"), t += "", e += "";
      var n = e.split(t);
      return "undefined" == typeof r ? n : (0 === r && (r = 1), r > 0 ? r >= n.length ? n : n.slice(0, r - 1).concat([n.slice(r - 1).join(t)]) : -r >= n.length ? [] : (n.splice(n.length + r), n));
    }
  }, {
    key: "length",
    value: function length(t) {
      var e = t + "",
          r = 0,
          n = "",
          o = 0;

      var i = function i(t, e) {
        var r = t.charCodeAt(e),
            n = "",
            o = "";

        if (r >= 55296 && 56319 >= r) {
          if (t.length <= e + 1) {
            throw "High surrogate without following low surrogate";
          }

          if (n = t.charCodeAt(e + 1), 56320 > n || n > 57343) {
            throw "High surrogate without following low surrogate";
          }

          return t.charAt(e) + t.charAt(e + 1);
        }

        if (r >= 56320 && 57343 >= r) {
          if (0 === e) {
            throw "Low surrogate without preceding high surrogate";
          }

          if (o = t.charCodeAt(e - 1), 55296 > o || o > 56319) {
            throw "Low surrogate without preceding high surrogate";
          }

          return !1;
        }

        return t.charAt(e);
      };

      for (r = 0, o = 0; r < e.length; r++) {
        (n = i(e, r)) !== !1 && o++;
      }

      return o;
    }
  }, {
    key: "trim",
    value: function trim(t, e) {
      var r,
          n = 0,
          o = 0;

      for (t += "", e ? (e += "", r = e.replace(/([\[\]\(\)\.\?\/\*\{\}\+\$\^\:])/g, "$1")) : r = " \n\r\t\f\x0B \u2000\u2001\u2002\u2003\u2004\u2005\u2006\u2007\u2008\u2009\u200A\u200B\u2028\u2029\u3000", n = t.length, o = 0; n > o; o++) {
        if (-1 === r.indexOf(t.charAt(o))) {
          t = t.substring(o);
          break;
        }
      }

      for (n = t.length, o = n - 1; o >= 0; o--) {
        if (-1 === r.indexOf(t.charAt(o))) {
          t = t.substring(0, o + 1);
          break;
        }
      }

      return -1 === r.indexOf(t.charAt(0)) ? t : "";
    }
  }, {
    key: "ucfirst",
    value: function ucfirst(t) {
      t += "";
      var e = t.charAt(0).toUpperCase();
      return e + t.substr(1);
    }
  }, {
    key: "replaceAll",
    value: function replaceAll(target, search, replacement) {
      return target.replace(new RegExp(search, 'g'), replacement);
    }
  }, {
    key: "replaceSingleWhiteSpaces",
    value: function replaceSingleWhiteSpaces(target, replacement) {
      return target.replace(/\s/g, replacement);
    }
  }, {
    key: "stripTags",
    value: function stripTags(input, allowed) {
      if (input === null) {
        input = '';
      } // making sure the allowed arg is a string containing only tags in lowercase (<a><b><c>)


      allowed = (((allowed || '') + '').toLowerCase().match(/<[a-z][a-z0-9]*>/g) || []).join('');
      var tags = /<\/?([a-z][a-z0-9]*)\b[^>]*>/gi;
      var commentsAndPhpTags = /<!--[\s\S]*?-->|<\?(?:php)?[\s\S]*?\?>/gi;
      return input.replace(commentsAndPhpTags, '').replace(tags, function ($0, $1) {
        return allowed.indexOf('<' + $1.toLowerCase() + '>') > -1 ? $0 : '';
      });
    }
  }, {
    key: "numberFormat",
    value: function numberFormat(t, e, r, n) {
      t = (t + "").replace(/[^0-9+\-Ee.]/g, "");

      var o = isFinite(+t) ? +t : 0,
          i = isFinite(+e) ? Math.abs(e) : 0,
          u = "undefined" === typeof n ? "," : n,
          a = "undefined" === typeof r ? "." : r,
          s = "",
          f = function f(t, e) {
        var r = Math.pow(10, e);
        return "" + (Math.round(t * r) / r).toFixed(e);
      };

      return s = (i ? f(o, i) : "" + Math.round(o)).split("."), s[0].length > 3 && (s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, u)), (s[1] || "").length < i && (s[1] = s[1] || "", s[1] += new Array(i - s[1].length + 1).join("0")), s.join(a);
    }
  }, {
    key: "utf8Encode",
    value: function utf8Encode(string) {
      string = string.replace(/\r\n/g, "\n");
      var utftext = "";

      for (var n = 0; n < string.length; n++) {
        var c = string.charCodeAt(n);

        if (c < 128) {
          utftext += String.fromCharCode(c);
        } else if (c > 127 && c < 2048) {
          utftext += String.fromCharCode(c >> 6 | 192);
          utftext += String.fromCharCode(c & 63 | 128);
        } else {
          utftext += String.fromCharCode(c >> 12 | 224);
          utftext += String.fromCharCode(c >> 6 & 63 | 128);
          utftext += String.fromCharCode(c & 63 | 128);
        }
      }

      return utftext;
    }
  }, {
    key: "utf8Decode",
    value: function utf8Decode(utftext) {
      var string = "",
          i = 0,
          c = 0,
          c1 = 0,
          c2 = 0,
          c3 = 0;

      while (i < utftext.length) {
        c = utftext.charCodeAt(i);

        if (c < 128) {
          string += String.fromCharCode(c);
          i++;
        } else if (c > 191 && c < 224) {
          c2 = utftext.charCodeAt(i + 1);
          string += String.fromCharCode((c & 31) << 6 | c2 & 63);
          i += 2;
        } else {
          c2 = utftext.charCodeAt(i + 1);
          c3 = utftext.charCodeAt(i + 2);
          string += String.fromCharCode((c & 15) << 12 | (c2 & 63) << 6 | c3 & 63);
          i += 3;
        }
      }

      return string;
    }
  }, {
    key: "extractFillData",
    value: function extractFillData() {
      var str = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '';
      var replacements = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
      var leftSeparator = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : ':';
      var rightSeparator = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : '';
      var r = [];

      for (var k in replacements) {
        if (replacements.hasOwnProperty(k)) {
          if (str.indexOf(leftSeparator + k + rightSeparator) > -1) {
            r.push(k);
          }
        }
      }

      return r;
    }
  }, {
    key: "fill",
    value: function fill() {
      var str = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '';
      var replacements = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
      var leftSeparator = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : ':';
      var rightSeparator = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : '';

      for (var k in replacements) {
        if (replacements.hasOwnProperty(k)) {
          str = str.replace(leftSeparator + k + rightSeparator, replacements[k]);
        }
      }

      return str;
    }
  }, {
    key: "generateRandomString",
    value: function generateRandomString() {
      var length = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 10;
      var result = '',
          characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789',
          charactersLength = characters.length;

      for (var i = 0; i < length; i++) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
      }

      return result;
    }
  }, {
    key: "htmlEntities",
    value: function htmlEntities(str) {
      return String(str).replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;');
    }
  }]);

  return StringHelper;
}();

/***/ }),

/***/ "./js/helpers/TimeHelper.js":
/*!**********************************!*\
  !*** ./js/helpers/TimeHelper.js ***!
  \**********************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "TimeHelper": () => (/* binding */ TimeHelper)
/* harmony export */ });
function _classCallCheck(instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
}

function _defineProperties(target, props) {
  for (var i = 0; i < props.length; i++) {
    var descriptor = props[i];
    descriptor.enumerable = descriptor.enumerable || false;
    descriptor.configurable = true;
    if ("value" in descriptor) descriptor.writable = true;
    Object.defineProperty(target, descriptor.key, descriptor);
  }
}

function _createClass(Constructor, protoProps, staticProps) {
  if (protoProps) _defineProperties(Constructor.prototype, protoProps);
  if (staticProps) _defineProperties(Constructor, staticProps);
  Object.defineProperty(Constructor, "prototype", {
    writable: false
  });
  return Constructor;
}

var TimeHelper = /*#__PURE__*/function () {
  function TimeHelper() {
    _classCallCheck(this, TimeHelper);
  }

  _createClass(TimeHelper, null, [{
    key: "date",
    value: function date(format, timestamp) {
      var jsdate, f; // Keep this here (works, but for code commented-out below for file size reasons)
      // let tal= [];

      var txtWords = ['Sun', 'Mon', 'Tues', 'Wednes', 'Thurs', 'Fri', 'Satur', 'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December']; // trailing backslash -> (dropped)
      // a backslash followed by any character (including backslash) -> the character
      // empty string -> empty string

      var formatChr = /\\?(.?)/gi;

      var formatChrCb = function formatChrCb(t, s) {
        return f[t] ? f[t]() : s;
      };

      var _pad = function _pad(n, c) {
        n = String(n);

        while (n.length < c) {
          n = '0' + n;
        }

        return n;
      };

      f = {
        // Day
        d: function d() {
          // Day of month w/leading 0; 01..31
          return _pad(f.j(), 2);
        },
        D: function D() {
          // Shorthand day name; Mon...Sun
          return f.l().slice(0, 3);
        },
        j: function j() {
          // Day of month; 1..31
          return jsdate.getDate();
        },
        l: function l() {
          // Full day name; Monday...Sunday
          return txtWords[f.w()] + 'day';
        },
        N: function N() {
          // ISO-8601 day of week; 1[Mon]..7[Sun]
          return f.w() || 7;
        },
        S: function S() {
          // Ordinal suffix for day of month; st, nd, rd, th
          var j = f.j();
          var i = j % 10;

          if (i <= 3 && parseInt(j % 100 / 10, 10) === 1) {
            i = 0;
          }

          return ['st', 'nd', 'rd'][i - 1] || 'th';
        },
        w: function w() {
          // Day of week; 0[Sun]..6[Sat]
          return jsdate.getDay();
        },
        z: function z() {
          // Day of year; 0..365
          var a = new Date(f.Y(), f.n() - 1, f.j());
          var b = new Date(f.Y(), 0, 1);
          return Math.round((a - b) / 864e5);
        },
        // Week
        W: function W() {
          // ISO-8601 week number
          var a = new Date(f.Y(), f.n() - 1, f.j() - f.N() + 3);
          var b = new Date(a.getFullYear(), 0, 4);
          return _pad(1 + Math.round((a - b) / 864e5 / 7), 2);
        },
        // Month
        F: function F() {
          // Full month name; January...December
          return txtWords[6 + f.n()];
        },
        m: function m() {
          // Month w/leading 0; 01...12
          return _pad(f.n(), 2);
        },
        M: function M() {
          // Shorthand month name; Jan...Dec
          return f.F().slice(0, 3);
        },
        n: function n() {
          // Month; 1...12
          return jsdate.getMonth() + 1;
        },
        t: function t() {
          // Days in month; 28...31
          return new Date(f.Y(), f.n(), 0).getDate();
        },
        // Year
        L: function L() {
          // Is leap year?; 0 or 1
          var j = f.Y();
          return j % 4 === 0 & j % 100 !== 0 | j % 400 === 0;
        },
        o: function o() {
          // ISO-8601 year
          var n = f.n();
          var W = f.W();
          var Y = f.Y();
          return Y + (n === 12 && W < 9 ? 1 : n === 1 && W > 9 ? -1 : 0);
        },
        Y: function Y() {
          // Full year; e.g. 1980...2010
          return jsdate.getFullYear();
        },
        y: function y() {
          // Last two digits of year; 00...99
          return f.Y().toString().slice(-2);
        },
        // Time
        a: function a() {
          // am or pm
          return jsdate.getHours() > 11 ? 'pm' : 'am';
        },
        A: function A() {
          // AM or PM
          return f.a().toUpperCase();
        },
        B: function B() {
          // Swatch Internet time; 000..999
          var H = jsdate.getUTCHours() * 36e2; // Hours

          var i = jsdate.getUTCMinutes() * 60; // Minutes
          // Seconds

          var s = jsdate.getUTCSeconds();
          return _pad(Math.floor((H + i + s + 36e2) / 86.4) % 1e3, 3);
        },
        g: function g() {
          // 12-Hours; 1..12
          return f.G() % 12 || 12;
        },
        G: function G() {
          // 24-Hours; 0..23
          return jsdate.getHours();
        },
        h: function h() {
          // 12-Hours w/leading 0; 01..12
          return _pad(f.g(), 2);
        },
        H: function H() {
          // 24-Hours w/leading 0; 00..23
          return _pad(f.G(), 2);
        },
        i: function i() {
          // Minutes w/leading 0; 00..59
          return _pad(jsdate.getMinutes(), 2);
        },
        s: function s() {
          // Seconds w/leading 0; 00..59
          return _pad(jsdate.getSeconds(), 2);
        },
        u: function u() {
          // Microseconds; 000000-999000
          return _pad(jsdate.getMilliseconds() * 1000, 6);
        },
        // Timezone
        e: function e() {
          // Timezone identifier; e.g. Atlantic/Azores, ...
          // The following works, but requires inclusion of the very large
          // timezone_abbreviations_list() function.

          /*              return that.date_default_timezone_get();
           */
          var msg = 'Not supported (see source code of date() for timezone on how to add support)';
          throw new Error(msg);
        },
        I: function I() {
          // DST observed?; 0 or 1
          // Compares Jan 1 minus Jan 1 UTC to Jul 1 minus Jul 1 UTC.
          // If they are not equal, then DST is observed.
          var a = new Date(f.Y(), 0); // Jan 1

          var c = Date.UTC(f.Y(), 0); // Jan 1 UTC

          var b = new Date(f.Y(), 6); // Jul 1
          // Jul 1 UTC

          var d = Date.UTC(f.Y(), 6);
          return a - c !== b - d ? 1 : 0;
        },
        O: function O() {
          // Difference to GMT in hour format; e.g. +0200
          var tzo = jsdate.getTimezoneOffset();
          var a = Math.abs(tzo);
          return (tzo > 0 ? '-' : '+') + _pad(Math.floor(a / 60) * 100 + a % 60, 4);
        },
        P: function P() {
          // Difference to GMT w/colon; e.g. +02:00
          var O = f.O();
          return O.substr(0, 3) + ':' + O.substr(3, 2);
        },
        T: function T() {
          // The following works, but requires inclusion of the very
          // large timezone_abbreviations_list() function.
          return 'UTC';
        },
        Z: function Z() {
          // Timezone offset in seconds (-43200...50400)
          return -jsdate.getTimezoneOffset() * 60;
        },
        // Full Date/Time
        c: function c() {
          // ISO-8601 date.
          return 'Y-m-d\\TH:i:sP'.replace(formatChr, formatChrCb);
        },
        r: function r() {
          // RFC 2822
          return 'D, d M Y H:i:s O'.replace(formatChr, formatChrCb);
        },
        U: function U() {
          // Seconds since UNIX epoch
          return jsdate / 1000 | 0;
        }
      };

      var _date = function _date(format, timestamp) {
        jsdate = timestamp === undefined ? new Date() // Not provided
        : timestamp instanceof Date ? new Date(timestamp) // JS Date()
        : new Date(timestamp * 1000) // UNIX timestamp (auto-convert to int)
        ;
        return format.replace(formatChr, formatChrCb);
      };

      return _date(format, timestamp);
    }
  }, {
    key: "debounce",
    value: function debounce(fn, delay) {
      var timer = null;
      return function () {
        var context = this,
            args = arguments;
        clearTimeout(timer);
        timer = setTimeout(function () {
          fn.apply(context, args);
        }, delay);
      };
    }
  }, {
    key: "sleep",
    value: function sleep(milliseconds) {
      var start = new Date().getTime();

      for (var i = 0; i < 1e7; i++) {
        if (new Date().getTime() - start > milliseconds) {
          break;
        }
      }
    }
  }, {
    key: "throttle",
    value: function throttle(fn, threshhold, scope) {
      threshhold || (threshhold = 250);
      var last, deferTimer;
      return function () {
        var context = scope || this;
        var now = +new Date(),
            args = arguments;

        if (last && now < last + threshhold) {
          // hold on to it
          clearTimeout(deferTimer);
          deferTimer = setTimeout(function () {
            last = now;
            fn.apply(context, args);
          }, threshhold);
        } else {
          last = now;
          fn.apply(context, args);
        }
      };
    }
  }, {
    key: "time",
    value: function time() {
      return new Date().getTime();
    }
  }, {
    key: "getStampInMilliseconds",
    value: function getStampInMilliseconds() {
      return Date.now();
    }
  }, {
    key: "isoToDate",
    value: function isoToDate(str) {
      var b = str.split(/\D/);
      return new Date(b[0], b[1] - 1, b[2], b[3], b[4], b[5]);
    }
  }]);

  return TimeHelper;
}();

/***/ }),

/***/ "./js/helpers/TypeHelper.js":
/*!**********************************!*\
  !*** ./js/helpers/TypeHelper.js ***!
  \**********************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "TypeHelper": () => (/* binding */ TypeHelper)
/* harmony export */ });
function _typeof(obj) {
  "@babel/helpers - typeof";

  return _typeof = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (obj) {
    return typeof obj;
  } : function (obj) {
    return obj && "function" == typeof Symbol && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj;
  }, _typeof(obj);
}

function _classCallCheck(instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
}

function _defineProperties(target, props) {
  for (var i = 0; i < props.length; i++) {
    var descriptor = props[i];
    descriptor.enumerable = descriptor.enumerable || false;
    descriptor.configurable = true;
    if ("value" in descriptor) descriptor.writable = true;
    Object.defineProperty(target, descriptor.key, descriptor);
  }
}

function _createClass(Constructor, protoProps, staticProps) {
  if (protoProps) _defineProperties(Constructor.prototype, protoProps);
  if (staticProps) _defineProperties(Constructor, staticProps);
  Object.defineProperty(Constructor, "prototype", {
    writable: false
  });
  return Constructor;
}

var TypeHelper = /*#__PURE__*/function () {
  function TypeHelper() {
    _classCallCheck(this, TypeHelper);
  }

  _createClass(TypeHelper, null, [{
    key: "isArray",
    value: function isArray(mixedVar) {
      if (typeof Array.isArray === 'undefined') {
        Array.isArray = function (obj) {
          return Object.prototype.toString.call(obj) === '[object Array]';
        };
      }

      return Array.isArray(mixedVar);
    }
  }, {
    key: "isFunction",
    value: function isFunction(fn) {
      return typeof fn === 'function';
    }
  }, {
    key: "isNumeric",
    value: function isNumeric(value) {
      var v = String(value).replace(',', '.'),
          v1 = parseFloat(v);
      return !isNaN(v1) && isFinite(v1);
    }
  }, {
    key: "isString",
    value: function isString(value) {
      return typeof value === 'string';
    }
  }, {
    key: "isObject",
    value: function isObject(mixedVar) {
      return _typeof(mixedVar) === 'object';
    }
  }, {
    key: "isUndefined",
    value: function isUndefined(mixedVar) {
      return typeof mixedVar === 'undefined';
    }
  }, {
    key: "is",
    value: function is(v) {
      return typeof v !== "undefined" || v !== null;
    }
  }, {
    key: "isset",
    value: function isset() {
      var a = arguments,
          l = a.length,
          i = 0,
          undef;

      if (l === 0) {
        throw new Error('Empty isset');
      }

      while (i !== l) {
        if (a[i] === undef || a[i] === null) {
          return false;
        }

        i++;
      }

      return true;
    }
  }]);

  return TypeHelper;
}();

/***/ }),

/***/ "./js/helpers/UrlHelper.js":
/*!*********************************!*\
  !*** ./js/helpers/UrlHelper.js ***!
  \*********************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "UrlHelper": () => (/* binding */ UrlHelper)
/* harmony export */ });
/* harmony import */ var _StringHelper__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./StringHelper */ "./js/helpers/StringHelper.js");
function _classCallCheck(instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
}

function _defineProperties(target, props) {
  for (var i = 0; i < props.length; i++) {
    var descriptor = props[i];
    descriptor.enumerable = descriptor.enumerable || false;
    descriptor.configurable = true;
    if ("value" in descriptor) descriptor.writable = true;
    Object.defineProperty(target, descriptor.key, descriptor);
  }
}

function _createClass(Constructor, protoProps, staticProps) {
  if (protoProps) _defineProperties(Constructor.prototype, protoProps);
  if (staticProps) _defineProperties(Constructor, staticProps);
  Object.defineProperty(Constructor, "prototype", {
    writable: false
  });
  return Constructor;
}


var UrlHelper = /*#__PURE__*/function () {
  function UrlHelper() {
    _classCallCheck(this, UrlHelper);
  }

  _createClass(UrlHelper, null, [{
    key: "decodeUrl",
    value: function decodeUrl(str) {
      return decodeURIComponent((str + '').replace(/%(?![\da-f]{2})/gi, function () {
        // PHP tolerates poorly formed escape sequences
        return '%25';
      }).replace(/\+/g, '%20'));
    }
  }, {
    key: "toSlug",
    value: function toSlug(str) {
      str = str.toLowerCase();
      str = _StringHelper__WEBPACK_IMPORTED_MODULE_0__.StringHelper.replaceSingleWhiteSpaces(str, '-');
      str = _StringHelper__WEBPACK_IMPORTED_MODULE_0__.StringHelper.replaceAll(str, 'ñ', 'n');
      str = _StringHelper__WEBPACK_IMPORTED_MODULE_0__.StringHelper.replaceAll(str, '\'', '');
      str = _StringHelper__WEBPACK_IMPORTED_MODULE_0__.StringHelper.replaceAll(str, '´', '');
      str = _StringHelper__WEBPACK_IMPORTED_MODULE_0__.StringHelper.replaceAll(str, '\\', '');
      str = _StringHelper__WEBPACK_IMPORTED_MODULE_0__.StringHelper.replaceAll(str, '.', '');
      str = _StringHelper__WEBPACK_IMPORTED_MODULE_0__.StringHelper.replaceAll(str, ',', '');
      str = _StringHelper__WEBPACK_IMPORTED_MODULE_0__.StringHelper.replaceAll(str, '/', '');
      str = _StringHelper__WEBPACK_IMPORTED_MODULE_0__.StringHelper.replaceAll(str, ':', '');
      str = _StringHelper__WEBPACK_IMPORTED_MODULE_0__.StringHelper.replaceAll(str, 'á', 'a');
      str = _StringHelper__WEBPACK_IMPORTED_MODULE_0__.StringHelper.replaceAll(str, 'é', 'e');
      str = _StringHelper__WEBPACK_IMPORTED_MODULE_0__.StringHelper.replaceAll(str, 'í', 'i');
      str = _StringHelper__WEBPACK_IMPORTED_MODULE_0__.StringHelper.replaceAll(str, 'ó', 'o');
      str = _StringHelper__WEBPACK_IMPORTED_MODULE_0__.StringHelper.replaceAll(str, 'ú', 'u');
      str = _StringHelper__WEBPACK_IMPORTED_MODULE_0__.StringHelper.replaceAll(str, 'à', 'a');
      str = _StringHelper__WEBPACK_IMPORTED_MODULE_0__.StringHelper.replaceAll(str, 'à', 'e');
      str = _StringHelper__WEBPACK_IMPORTED_MODULE_0__.StringHelper.replaceAll(str, 'à', 'i');
      str = _StringHelper__WEBPACK_IMPORTED_MODULE_0__.StringHelper.replaceAll(str, 'à', 'o');
      str = _StringHelper__WEBPACK_IMPORTED_MODULE_0__.StringHelper.replaceAll(str, 'à', 'u');
      str = _StringHelper__WEBPACK_IMPORTED_MODULE_0__.StringHelper.replaceAll(str, 'ẃ', 'w');
      str = _StringHelper__WEBPACK_IMPORTED_MODULE_0__.StringHelper.replaceAll(str, 'ẁ', 'w');
      str = _StringHelper__WEBPACK_IMPORTED_MODULE_0__.StringHelper.replaceAll(str, 'ý', 'y');
      str = _StringHelper__WEBPACK_IMPORTED_MODULE_0__.StringHelper.replaceAll(str, 'ỳ', 'y');
      str = _StringHelper__WEBPACK_IMPORTED_MODULE_0__.StringHelper.replaceAll(str, 'ś', 's');
      str = _StringHelper__WEBPACK_IMPORTED_MODULE_0__.StringHelper.replaceAll(str, 'ǵ', 'g');
      str = _StringHelper__WEBPACK_IMPORTED_MODULE_0__.StringHelper.replaceAll(str, 'ḱ', 'k');
      str = _StringHelper__WEBPACK_IMPORTED_MODULE_0__.StringHelper.replaceAll(str, 'ĺ', 'l');
      str = _StringHelper__WEBPACK_IMPORTED_MODULE_0__.StringHelper.replaceAll(str, 'ź', 'z');
      str = _StringHelper__WEBPACK_IMPORTED_MODULE_0__.StringHelper.replaceAll(str, 'ć', 'c');
      str = _StringHelper__WEBPACK_IMPORTED_MODULE_0__.StringHelper.replaceAll(str, 'ǘ', 'v');
      str = _StringHelper__WEBPACK_IMPORTED_MODULE_0__.StringHelper.replaceAll(str, 'ń', 'n');
      str = _StringHelper__WEBPACK_IMPORTED_MODULE_0__.StringHelper.replaceAll(str, 'ḿ', 'm');
      str = _StringHelper__WEBPACK_IMPORTED_MODULE_0__.StringHelper.replaceAll(str, 'ǹ', 'n');
      str = _StringHelper__WEBPACK_IMPORTED_MODULE_0__.StringHelper.replaceAll(str, 'ǜ', 'v');
      str = _StringHelper__WEBPACK_IMPORTED_MODULE_0__.StringHelper.replaceAll(str, 'ä', 'a');
      str = _StringHelper__WEBPACK_IMPORTED_MODULE_0__.StringHelper.replaceAll(str, 'ë', 'e');
      str = _StringHelper__WEBPACK_IMPORTED_MODULE_0__.StringHelper.replaceAll(str, 'ï', 'i');
      str = _StringHelper__WEBPACK_IMPORTED_MODULE_0__.StringHelper.replaceAll(str, 'ö', 'o');
      str = _StringHelper__WEBPACK_IMPORTED_MODULE_0__.StringHelper.replaceAll(str, 'ü', 'u');
      str = _StringHelper__WEBPACK_IMPORTED_MODULE_0__.StringHelper.replaceAll(str, 'ẅ', 'w');
      str = _StringHelper__WEBPACK_IMPORTED_MODULE_0__.StringHelper.replaceAll(str, 'ÿ', 'y');
      str = _StringHelper__WEBPACK_IMPORTED_MODULE_0__.StringHelper.replaceAll(str, 'ẍ', 'x');
      return str;
    }
  }]);

  return UrlHelper;
}();

/***/ }),

/***/ "./js/helpers/validations/EmailValidation.js":
/*!***************************************************!*\
  !*** ./js/helpers/validations/EmailValidation.js ***!
  \***************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "EmailValidation": () => (/* binding */ EmailValidation)
/* harmony export */ });
/* harmony import */ var _classes_form_Validation__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../classes/form/Validation */ "./js/classes/form/Validation.js");

var EmailValidation = new _classes_form_Validation__WEBPACK_IMPORTED_MODULE_0__.Validation('email', function (value) {
  return new RegExp('^[-!#$%&\'*+\\./0-9=?A-Z^_`a-z{|}~]+@[-!#$%&\'*+\\/0-9=?A-Z^_`a-z{|}~]+\.[-!#$%&\'*+\\./0-9=?A-Z^_`a-z{|}~]+$').test(value);
});

/***/ }),

/***/ "./js/helpers/validations/EmptyValidation.js":
/*!***************************************************!*\
  !*** ./js/helpers/validations/EmptyValidation.js ***!
  \***************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "EmptyValidation": () => (/* binding */ EmptyValidation)
/* harmony export */ });
/* harmony import */ var _classes_form_Validation__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../classes/form/Validation */ "./js/classes/form/Validation.js");

var EmptyValidation = new _classes_form_Validation__WEBPACK_IMPORTED_MODULE_0__.Validation('', function (value) {
  return true;
});

/***/ }),

/***/ "./js/helpers/validations/NotEmptyValidation.js":
/*!******************************************************!*\
  !*** ./js/helpers/validations/NotEmptyValidation.js ***!
  \******************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "NotEmptyValidation": () => (/* binding */ NotEmptyValidation)
/* harmony export */ });
/* harmony import */ var _classes_form_Validation__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../classes/form/Validation */ "./js/classes/form/Validation.js");

var NotEmptyValidation = new _classes_form_Validation__WEBPACK_IMPORTED_MODULE_0__.Validation('notEmpty', function (value) {
  if (typeof value === 'undefined') {
    return false;
  }

  return String(value) !== '';
});

/***/ }),

/***/ "./js/http/Environment.js":
/*!********************************!*\
  !*** ./js/http/Environment.js ***!
  \********************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Environment": () => (/* binding */ Environment)
/* harmony export */ });
/* harmony import */ var _classes_EnvironmentInstance__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./classes/EnvironmentInstance */ "./js/http/classes/EnvironmentInstance.js");

var Environment = {
  DEFAULT_DATA: {
    name: undefined,
    url: undefined,
    auth: {
      user: undefined,
      password: undefined
    }
  },
  INSTANCES: {},
  instance: _classes_EnvironmentInstance__WEBPACK_IMPORTED_MODULE_0__.EnvironmentInstance,
  create: function create() {
    var data = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : Environment.DEFAULT_DATA;
    var r = new Environment.instance(data);
    Environment.INSTANCES[r.name] = r;
    return Environment;
  },
  get: function get() {
    var name = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '';
    return Environment.INSTANCES[name];
  }
};

/***/ }),

/***/ "./js/http/Resource.js":
/*!*****************************!*\
  !*** ./js/http/Resource.js ***!
  \*****************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Resource": () => (/* binding */ Resource)
/* harmony export */ });
/* harmony import */ var _classes_ResourceInstance__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./classes/ResourceInstance */ "./js/http/classes/ResourceInstance.js");
/* harmony import */ var _helpers_ObjectHelper__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../helpers/ObjectHelper */ "./js/helpers/ObjectHelper.js");


var Resource = {
  DEFAULT_DATA: {
    name: undefined,
    url: undefined,
    method: undefined,
    environment: undefined,
    unsafeParams: false,
    params: {},
    renameParams: {},
    fillLeftSeparator: '{',
    fillRightSeparator: '}',
    success: undefined,
    validStatuses: [200, 201, 202],
    searchParam: undefined,
    processResults: undefined,
    escapeMarkup: undefined,
    templateResult: undefined,
    templateSelection: undefined,
    paginationVariable: 'paged',
    enableMultipleCalling: false,
    isFileUpload: false,
    lastPageChecker: function lastPageChecker() {
      return false;
    },
    dataType: 'json',
    cache: {},
    cacheTime: 0
  },
  INSTANCES: {},
  instance: _classes_ResourceInstance__WEBPACK_IMPORTED_MODULE_0__.ResourceInstance,
  create: function create() {
    var data = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : Resource.DEFAULT_DATA;
    var r = new Resource.instance(data);
    Resource.INSTANCES[r.name] = r;
    return Resource;
  },
  createMany: function createMany() {
    var resources = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : [];
    resources.forEach(function (resource) {
      Resource.create(resource);
    });
  },
  get: function get() {
    var name = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '';
    return _helpers_ObjectHelper__WEBPACK_IMPORTED_MODULE_1__.ObjectHelper.clone(Resource.INSTANCES[name]);
  },
  paramsToString: function paramsToString() {
    var params = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
    var r = [];

    for (var key in params) {
      if (params.hasOwnProperty(key)) {
        if (Array.isArray(params[key])) {
          if (params[key].length > 0) {
            r.push(key + '=' + JSON.stringify(params[key]) + '');
          }
        } else {
          r.push(key + '=' + params[key]);
        }
      }
    }

    return r.join('&');
  }
};

/***/ }),

/***/ "./js/http/classes/EnvironmentInstance.js":
/*!************************************************!*\
  !*** ./js/http/classes/EnvironmentInstance.js ***!
  \************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "EnvironmentInstance": () => (/* binding */ EnvironmentInstance)
/* harmony export */ });
function _defineProperties(target, props) {
  for (var i = 0; i < props.length; i++) {
    var descriptor = props[i];
    descriptor.enumerable = descriptor.enumerable || false;
    descriptor.configurable = true;
    if ("value" in descriptor) descriptor.writable = true;
    Object.defineProperty(target, descriptor.key, descriptor);
  }
}

function _createClass(Constructor, protoProps, staticProps) {
  if (protoProps) _defineProperties(Constructor.prototype, protoProps);
  if (staticProps) _defineProperties(Constructor, staticProps);
  Object.defineProperty(Constructor, "prototype", {
    writable: false
  });
  return Constructor;
}

function _classCallCheck(instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
}

var EnvironmentInstance = /*#__PURE__*/_createClass(function EnvironmentInstance() {
  var data = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};

  _classCallCheck(this, EnvironmentInstance);

  this.name = data.name;
  this.url = data.url;
  this.auth = data.auth;
});

/***/ }),

/***/ "./js/http/classes/ResourceInstance.js":
/*!*********************************************!*\
  !*** ./js/http/classes/ResourceInstance.js ***!
  \*********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ResourceInstance": () => (/* binding */ ResourceInstance)
/* harmony export */ });
/* harmony import */ var _helpers_ObjectHelper__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../helpers/ObjectHelper */ "./js/helpers/ObjectHelper.js");
/* harmony import */ var _helpers_TypeHelper__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../helpers/TypeHelper */ "./js/helpers/TypeHelper.js");
/* harmony import */ var _Environment__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../Environment */ "./js/http/Environment.js");
/* harmony import */ var _Resource__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../Resource */ "./js/http/Resource.js");
/* harmony import */ var _helpers_StringHelper__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../helpers/StringHelper */ "./js/helpers/StringHelper.js");
/* harmony import */ var _helpers_TimeHelper__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../helpers/TimeHelper */ "./js/helpers/TimeHelper.js");






function ResourceInstance() {
  var data = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
  data = _helpers_ObjectHelper__WEBPACK_IMPORTED_MODULE_0__.ObjectHelper.merge(_helpers_ObjectHelper__WEBPACK_IMPORTED_MODULE_0__.ObjectHelper.clone(_Resource__WEBPACK_IMPORTED_MODULE_3__.Resource.DEFAULT_DATA), data);
  this.name = data.name;
  this.url = data.url;
  this.method = data.method;
  this.params = data.params;
  this.environment = data.environment;
  this.unsafeParams = data.unsafeParams;
  this.renameParams = data.renameParams;
  this.fillLeftSeparator = data.fillLeftSeparator;
  this.fillRightSeparator = data.fillRightSeparator;
  this.success = data.success;
  this.validStatuses = data.validStatuses;
  this.searchParam = data.searchParam;
  this.processResults = data.processResults;
  this.dataType = data.dataType;
  this.paginationVariable = data.paginationVariable;
  this.enableMultipleCalling = data.enableMultipleCalling;
  this.lastPageChecker = data.lastPageChecker;
  this.currentPage = undefined;
  this.isFetching = false;
  this.inLastPage = false;
  this.escapeMarkup = data.escapeMarkup;
  this.templateResult = data.templateResult;
  this.templateSelection = data.templateSelection;
  this.isFileUpload = data.isFileUpload;
  this.cache = {};
  this.cacheTime = data.cacheTime;
  this.forceRefreshFlag = false;

  this.forceRefresh = function () {
    this.forceRefreshFlag = true;
  };

  this.page = function () {
    var _this = this;

    var page = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : undefined;
    var params = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
    var environment = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : this.environment;

    if (this.isFetching && !this.enableMultipleCalling) {
      return new Promise(function (resolve, reject) {
        resolve(undefined);
      });
    }

    if (_helpers_TypeHelper__WEBPACK_IMPORTED_MODULE_1__.TypeHelper.isUndefined(page) && _helpers_TypeHelper__WEBPACK_IMPORTED_MODULE_1__.TypeHelper.isUndefined(this.currentPage)) {
      page = 1;
    } else if (_helpers_TypeHelper__WEBPACK_IMPORTED_MODULE_1__.TypeHelper.isUndefined(page) && !_helpers_TypeHelper__WEBPACK_IMPORTED_MODULE_1__.TypeHelper.isUndefined(this.currentPage)) {
      page = this.currentPage;
    }

    this.currentPage = page;

    if (this.inLastPage && this.currentPage !== 1) {
      return new Promise(function (resolve, reject) {
        resolve(undefined);
      });
    }

    params[this.paginationVariable] = this.currentPage;
    var data = this.build(params, environment);

    switch (data.method) {
      case 'get':
      case 'post':
        this.isFetching = true;
        return axios(data).then(function (promise) {
          ++_this.currentPage;
          _this.isFetching = false;
          _this.inLastPage = _this.lastPageChecker(promise);

          if (_helpers_TypeHelper__WEBPACK_IMPORTED_MODULE_1__.TypeHelper.isFunction(_this.success)) {
            return _this.success(promise);
          }

          return promise;
        })["catch"](function (error) {
          _this.isFetching = false;
          _this.inLastPage = _this.lastPageChecker(promise);
          return error;
        });

      default:
        console.warn('Error: Invalid method');
    }
  };

  this.call = function () {
    var _this2 = this;

    var params = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
    var environment = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : this.environment;

    if (this.isFetching && !this.enableMultipleCalling) {
      return new Promise(function (resolve, reject) {
        resolve(undefined);
      });
    }

    var data = this.build(params, environment);

    if (data.method === 'get' && this.cacheTime > 0 && !this.forceRefreshFlag && this.cache[data.url]) {
      var now = _helpers_TimeHelper__WEBPACK_IMPORTED_MODULE_5__.TimeHelper.time();
      var limit = this.cache[data.url].moment + this.cacheTime;

      if (limit - now > 0) {
        return new Promise(function (resolve, reject) {
          return resolve(function () {
            if (_helpers_TypeHelper__WEBPACK_IMPORTED_MODULE_1__.TypeHelper.isFunction(_this2.success)) {
              return _this2.success(_this2.cache[data.url].r);
            }

            return _this2.cache[data.url].r;
          }());
        });
      }
    }

    switch (data.method) {
      case 'get':
      case 'post':
      case 'put':
      case 'delete':
        this.isFetching = true;
        return axios(data).then(function (promise) {
          _this2.isFetching = false;

          if (data.method === 'get' && _this2.cacheTime > 0) {
            _this2.cache[data.url] = {
              moment: _helpers_TimeHelper__WEBPACK_IMPORTED_MODULE_5__.TimeHelper.time(),
              r: promise
            };
            _this2.forceRefreshFlag = false;
          }

          if (_helpers_TypeHelper__WEBPACK_IMPORTED_MODULE_1__.TypeHelper.isFunction(_this2.success)) {
            return _this2.success(promise);
          }

          return promise;
        })["catch"](function (error) {
          _this2.isFetching = false;
          return Promise.reject(new Error(error));
        });

      case 'download':
      case 'open':
        return axios.get(data.url, {
          'responseType': 'blob'
        }).then(function (r) {
          var contentDisposition = r.headers['content-disposition'],
              fileName = '';

          if (contentDisposition) {
            contentDisposition = contentDisposition.split(';');
            contentDisposition.forEach(function (z) {
              var y = z.split('=');

              if (_helpers_StringHelper__WEBPACK_IMPORTED_MODULE_4__.StringHelper.trim(y[0]) === 'filename') {
                var n = _helpers_StringHelper__WEBPACK_IMPORTED_MODULE_4__.StringHelper.trim(y[1]);
                n = _helpers_StringHelper__WEBPACK_IMPORTED_MODULE_4__.StringHelper.trim(n, '"');
                fileName = n;
              }
            });
          }

          window.download(r.data, fileName);

          if (_helpers_TypeHelper__WEBPACK_IMPORTED_MODULE_1__.TypeHelper.isFunction(_this2.success)) {
            return _this2.success(promise);
          }

          return r;
        })["catch"](function (error) {
          return error;
        });

      default:
        console.warn('Error: Invalid method');
    }
  };

  this.build = function () {
    var _this3 = this;

    var args = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
    var environment = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : undefined;
    var p = _helpers_ObjectHelper__WEBPACK_IMPORTED_MODULE_0__.ObjectHelper.clone(this.params);
    var r = {};

    for (var k in p) {
      if (p.hasOwnProperty(k)) {
        r[k] = p[k]["default"];
      }
    }

    for (var key in args) {
      if (this.unsafeParams || key === this.paginationVariable || args.hasOwnProperty(key) && this.params.hasOwnProperty(key)) {
        if (this.renameParams.hasOwnProperty(key)) {
          delete r[key];
          r[this.renameParams[key]] = args[key];
        } else {
          r[key] = args[key];
        }

        if (_helpers_TypeHelper__WEBPACK_IMPORTED_MODULE_1__.TypeHelper.isUndefined(r[key])) {
          delete r[key];
        }
      }
    }

    var url = this.url;
    var env = _Environment__WEBPACK_IMPORTED_MODULE_2__.Environment.get(environment);
    var auth = {};

    if (env && env.url) {
      url = env.url + url;

      if (!_helpers_TypeHelper__WEBPACK_IMPORTED_MODULE_1__.TypeHelper.isUndefined(env.auth) && !_helpers_TypeHelper__WEBPACK_IMPORTED_MODULE_1__.TypeHelper.isUndefined(env.auth.user)) {
        auth = env.auth;
      }
    }

    var toDelete = _helpers_StringHelper__WEBPACK_IMPORTED_MODULE_4__.StringHelper.extractFillData(url, r, this.fillLeftSeparator, this.fillRightSeparator);
    var link = _helpers_StringHelper__WEBPACK_IMPORTED_MODULE_4__.StringHelper.fill(url, r, this.fillLeftSeparator, this.fillRightSeparator);
    r = _helpers_ObjectHelper__WEBPACK_IMPORTED_MODULE_0__.ObjectHelper.deleteKeys(r, toDelete); // let _url = new URL(document.location.href.split('?')[0]);
    //
    // if (link.indexOf('/') === 0){
    //     link = _url.origin + link;
    // }

    if (this.method === 'get' || this.method === 'open') {
      var stringParams = _Resource__WEBPACK_IMPORTED_MODULE_3__.Resource.paramsToString(r);
      link = [link, stringParams].join('?');
      r = {};
    }

    var headers = undefined;

    if (this.isFileUpload) {
      headers = {
        'Content-Type': 'multipart/form-data'
      };
      var formData = new FormData();

      for (var _k in r) {
        if (r.hasOwnProperty(_k)) {
          formData.append(_k, r[_k]);
        }
      }

      r = formData;
    }

    return {
      url: link,
      method: this.method.toLowerCase(),
      data: r,
      auth: auth,
      validateStatus: function validateStatus(status) {
        return _this3.validateStatus(status);
      },
      headers: headers
    };
  };

  this.validateStatus = function (status) {
    if (this.validStatuses.length === 0) {
      return true;
    }

    return this.validStatuses.indexOf(status) !== -1;
  };
}

/***/ }),

/***/ "./lkt-ts/modules/lkt-helpers.js":
/*!***************************************!*\
  !*** ./lkt-ts/modules/lkt-helpers.js ***!
  \***************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "LktHelpers": () => (/* binding */ LktHelpers)
/* harmony export */ });
/* harmony import */ var _js_helpers_ArrayHelper__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../js/helpers/ArrayHelper */ "./js/helpers/ArrayHelper.js");
/* harmony import */ var _js_helpers_DocumentHelper__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../js/helpers/DocumentHelper */ "./js/helpers/DocumentHelper.js");
/* harmony import */ var _js_helpers_FileHelper__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../js/helpers/FileHelper */ "./js/helpers/FileHelper.js");
/* harmony import */ var _js_helpers_FunctionHelper__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../js/helpers/FunctionHelper */ "./js/helpers/FunctionHelper.js");
/* harmony import */ var _js_helpers_ImageHelper__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../js/helpers/ImageHelper */ "./js/helpers/ImageHelper.js");
/* harmony import */ var _js_helpers_NumberHelper__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../js/helpers/NumberHelper */ "./js/helpers/NumberHelper.js");
/* harmony import */ var _js_helpers_ObjectHelper__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../js/helpers/ObjectHelper */ "./js/helpers/ObjectHelper.js");
/* harmony import */ var _js_helpers_SocialShareHelper__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../js/helpers/SocialShareHelper */ "./js/helpers/SocialShareHelper.js");
/* harmony import */ var _js_helpers_StringHelper__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../js/helpers/StringHelper */ "./js/helpers/StringHelper.js");
/* harmony import */ var _js_dom_ScrollHelper__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../js/dom/ScrollHelper */ "./js/dom/ScrollHelper.js");
/* harmony import */ var _js_helpers_TimeHelper__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../../js/helpers/TimeHelper */ "./js/helpers/TimeHelper.js");
/* harmony import */ var _js_helpers_UrlHelper__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../../js/helpers/UrlHelper */ "./js/helpers/UrlHelper.js");
/* harmony import */ var _js_helpers_TypeHelper__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ../../js/helpers/TypeHelper */ "./js/helpers/TypeHelper.js");
/* harmony import */ var _js_helpers_NavigatorHelper__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ../../js/helpers/NavigatorHelper */ "./js/helpers/NavigatorHelper.js");














function LktHelpers(lkt) {
  lkt.Helper = lkt.Helper || {};
  lkt.Helper.Array = _js_helpers_ArrayHelper__WEBPACK_IMPORTED_MODULE_0__.ArrayHelper;
  lkt.Helper.Document = _js_helpers_DocumentHelper__WEBPACK_IMPORTED_MODULE_1__.DocumentHelper;
  lkt.Helper.File = _js_helpers_FileHelper__WEBPACK_IMPORTED_MODULE_2__.FileHelper;
  lkt.Helper.Function = _js_helpers_FunctionHelper__WEBPACK_IMPORTED_MODULE_3__.FunctionHelper;
  lkt.Helper.Image = _js_helpers_ImageHelper__WEBPACK_IMPORTED_MODULE_4__.ImageHelper;
  lkt.Helper.Navigator = _js_helpers_NavigatorHelper__WEBPACK_IMPORTED_MODULE_13__.NavigatorHelper;
  lkt.Helper.Number = _js_helpers_NumberHelper__WEBPACK_IMPORTED_MODULE_5__.NumberHelper;
  lkt.Helper.Object = _js_helpers_ObjectHelper__WEBPACK_IMPORTED_MODULE_6__.ObjectHelper;
  lkt.Helper.SocialShare = _js_helpers_SocialShareHelper__WEBPACK_IMPORTED_MODULE_7__.SocialShareHelper;
  lkt.Helper.String = _js_helpers_StringHelper__WEBPACK_IMPORTED_MODULE_8__.StringHelper;
  lkt.Helper.Scroll = _js_dom_ScrollHelper__WEBPACK_IMPORTED_MODULE_9__.ScrollHelper;
  lkt.Helper.Time = _js_helpers_TimeHelper__WEBPACK_IMPORTED_MODULE_10__.TimeHelper;
  lkt.Helper.Type = _js_helpers_TypeHelper__WEBPACK_IMPORTED_MODULE_12__.TypeHelper;
  lkt.Helper.Url = _js_helpers_UrlHelper__WEBPACK_IMPORTED_MODULE_11__.UrlHelper;
}

/***/ }),

/***/ "./lkt-ts/modules/lkt-http.js":
/*!************************************!*\
  !*** ./lkt-ts/modules/lkt-http.js ***!
  \************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "LktHttp": () => (/* binding */ LktHttp)
/* harmony export */ });
/* harmony import */ var _js_http_Environment__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../js/http/Environment */ "./js/http/Environment.js");
/* harmony import */ var _js_http_classes_EnvironmentInstance__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../js/http/classes/EnvironmentInstance */ "./js/http/classes/EnvironmentInstance.js");
/* harmony import */ var _js_http_Resource__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../js/http/Resource */ "./js/http/Resource.js");
/* harmony import */ var _js_http_classes_ResourceInstance__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../js/http/classes/ResourceInstance */ "./js/http/classes/ResourceInstance.js");




function LktHttp(lkt) {
  lkt.Http = lkt.Http || {};
  lkt.Http.Environment = _js_http_Environment__WEBPACK_IMPORTED_MODULE_0__.Environment;
  lkt.Http.EnvironmentInstance = _js_http_classes_EnvironmentInstance__WEBPACK_IMPORTED_MODULE_1__.EnvironmentInstance;
  lkt.Http.Resource = _js_http_Resource__WEBPACK_IMPORTED_MODULE_2__.Resource;
  lkt.Http.ResourceInstance = _js_http_classes_ResourceInstance__WEBPACK_IMPORTED_MODULE_3__.ResourceInstance;
}

/***/ })

/******/ 	});
/************************************************************************/
/******/ 	// The module cache
/******/ 	var __webpack_module_cache__ = {};
/******/ 	
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/ 		// Check if module is in cache
/******/ 		var cachedModule = __webpack_module_cache__[moduleId];
/******/ 		if (cachedModule !== undefined) {
/******/ 			return cachedModule.exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = __webpack_module_cache__[moduleId] = {
/******/ 			// no module.id needed
/******/ 			// no module.loaded needed
/******/ 			exports: {}
/******/ 		};
/******/ 	
/******/ 		// Execute the module function
/******/ 		__webpack_modules__[moduleId](module, module.exports, __webpack_require__);
/******/ 	
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/ 	
/************************************************************************/
/******/ 	/* webpack/runtime/define property getters */
/******/ 	(() => {
/******/ 		// define getter functions for harmony exports
/******/ 		__webpack_require__.d = (exports, definition) => {
/******/ 			for(var key in definition) {
/******/ 				if(__webpack_require__.o(definition, key) && !__webpack_require__.o(exports, key)) {
/******/ 					Object.defineProperty(exports, key, { enumerable: true, get: definition[key] });
/******/ 				}
/******/ 			}
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/hasOwnProperty shorthand */
/******/ 	(() => {
/******/ 		__webpack_require__.o = (obj, prop) => (Object.prototype.hasOwnProperty.call(obj, prop))
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/make namespace object */
/******/ 	(() => {
/******/ 		// define __esModule on exports
/******/ 		__webpack_require__.r = (exports) => {
/******/ 			if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 				Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 			}
/******/ 			Object.defineProperty(exports, '__esModule', { value: true });
/******/ 		};
/******/ 	})();
/******/ 	
/************************************************************************/
var __webpack_exports__ = {};
// This entry need to be wrapped in an IIFE because it need to be isolated against other modules in the chunk.
(() => {
/*!***********************!*\
  !*** ./lkt-ts/lkt.ts ***!
  \***********************/
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "lkt": () => (/* binding */ lkt)
/* harmony export */ });
/* harmony import */ var _modules_lkt_helpers__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./modules/lkt-helpers */ "./lkt-ts/modules/lkt-helpers.js");
/* harmony import */ var _modules_lkt_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./modules/lkt-http */ "./lkt-ts/modules/lkt-http.js");
/* harmony import */ var _js_helpers_validations_EmptyValidation__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../js/helpers/validations/EmptyValidation */ "./js/helpers/validations/EmptyValidation.js");
/* harmony import */ var _js_helpers_validations_NotEmptyValidation__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../js/helpers/validations/NotEmptyValidation */ "./js/helpers/validations/NotEmptyValidation.js");
/* harmony import */ var _js_helpers_validations_EmailValidation__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../js/helpers/validations/EmailValidation */ "./js/helpers/validations/EmailValidation.js");
// import {Lkt} from "./modules/lkt";






var Lkt =
/** @class */
function () {
  function Lkt() {
    this.Helper = {
      Array: undefined,
      Document: undefined,
      File: undefined,
      Function: undefined,
      Image: undefined,
      Navigator: undefined,
      Number: undefined,
      Object: undefined,
      SocialShare: undefined,
      String: undefined,
      Time: undefined,
      Type: undefined,
      Url: undefined
    };
    this.Http = {
      Environment: undefined,
      EnvironmentInstance: undefined,
      Resource: undefined,
      ResourceInstance: undefined
    };
    this.Sort = {};
    this.Validations = {
      Email: _js_helpers_validations_EmailValidation__WEBPACK_IMPORTED_MODULE_4__.EmailValidation,
      Empty: _js_helpers_validations_EmptyValidation__WEBPACK_IMPORTED_MODULE_2__.EmptyValidation,
      NotEmpty: _js_helpers_validations_NotEmptyValidation__WEBPACK_IMPORTED_MODULE_3__.NotEmptyValidation
    };
  }

  return Lkt;
}();

var _lkt = new Lkt();

(0,_modules_lkt_helpers__WEBPACK_IMPORTED_MODULE_0__.LktHelpers)(_lkt);
(0,_modules_lkt_http__WEBPACK_IMPORTED_MODULE_1__.LktHttp)(_lkt);
var lkt = _lkt;
})();

/******/ })()
;